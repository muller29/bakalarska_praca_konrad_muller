const gulp = require('gulp');

gulp.task('default', function () {
    gulp.src('./node_modules/sweetalert2/dist/sweetalert2.js')
        .pipe(gulp.dest('./public/dist/js'));
    gulp.src('./node_modules/sweetalert2/dist/sweetalert2.css')
        .pipe(gulp.dest('./public/dist/css'));
});