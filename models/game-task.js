const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const GameTask = sequelize.define('gameTask', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
});

module.exports = GameTask;