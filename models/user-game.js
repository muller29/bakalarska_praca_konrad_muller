const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const UserGame = sequelize.define('userGame', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    state: {
        type: Sequelize.TEXT,
        allowNull: true,
    },
    finished: {
        type: Sequelize.BOOLEAN,
        default: false
    },
    score: {
        type: Sequelize.INTEGER,
        default: 0
    },
    taskId: {
        type: Sequelize.INTEGER,
    },
    groupId: {
        type: Sequelize.INTEGER,
    }
});

module.exports = UserGame;