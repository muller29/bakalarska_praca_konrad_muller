const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const Game = sequelize.define('game', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    word: {
        type: Sequelize.STRING,
        allowNull: false
    },
    rows: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    columns: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    barrier: {
        type: Sequelize.BOOLEAN,
        default: false
    },
    maxTime: {
        type: Sequelize.INTEGER,
    },
    level: {
        type: Sequelize.STRING,
        allowNull: false
    },
    createdById: {
        type: Sequelize.INTEGER,
    }
});

module.exports = Game;