const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const GroupTask = sequelize.define('groupTask', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
});

module.exports = GroupTask;