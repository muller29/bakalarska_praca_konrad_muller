'use strict';

const Role = require('../models/role');
const bcrypt = require('bcryptjs');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const role = await Role.findOne({where: { name: "admin"}});
        const pass = await bcrypt.hash("admin", 12);

        await queryInterface.bulkInsert('users', [{
            firstName: 'Admin',
            lastName: 'Admin',
            email: 'admin@admin.sk',
            password: pass,
            roleId: role.id,
            createdAt: new Date(),
            updatedAt: new Date()
        }], {});
    },

    down: async (queryInterface, Sequelize) => {

        await queryInterface.bulkDelete('users', null, {});

    }
};
