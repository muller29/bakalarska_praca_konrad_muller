const path = require('path');

const express = require('express');

const studentController = require('../controllers/student');
const isAuth = require('../middleware/is-auth');
const isStudent = require('../middleware/isStudent');
const redirectIfLoggedIn = require('../middleware/redirectIfLoggedIn');

const router = express.Router();

router.get('/', redirectIfLoggedIn, studentController.getIndex);
router.get('/ulohy', isAuth, isStudent, studentController.getDashboard);
router.get('/skupiny/:groupId/ulohy/:taskId', isAuth, isStudent, studentController.getTaskPage);
router.post('/ulohy', isAuth, isStudent, studentController.postTask);
router.get('/groups-with-tasks', isAuth, isStudent, studentController.getGroupsWithTasks);
router.post('/game/save', isAuth, isStudent, studentController.postSaveGame);

module.exports = router;