const express = require('express');

const authController = require('../controllers/auth');
const redirectIfLoggedIn = require('../middleware/redirectIfLoggedIn');

const router = express.Router();


router.get('/admin', redirectIfLoggedIn, authController.getAdminLogin);
router.post('/login', authController.postLogin);

router.post('/logout', authController.postLogout);

module.exports = router;