const path = require('path');

const express = require('express');
const {check, body} = require('express-validator/check');

const adminController = require('../controllers/admin');
const User = require('../models/user');
const Group = require('../models/group');
const isAuth = require('../middleware/is-auth');
const isAdmin = require('../middleware/onlyAdmin');
const adminOrTeacher = require('../middleware/isAdminOrIsTeacher');
const allowCrudForTeacher = require('../middleware/allowCrudForTeacher');

const Sequelize = require('sequelize');
const Op = Sequelize.Op

const router = express.Router();

router.get('/dashboard', isAuth, adminOrTeacher, adminController.getDashboard);
router.get('/statistics', isAuth, adminOrTeacher, adminController.getStatistics);

router.get('/groups', isAuth, adminOrTeacher, adminController.getGroupsPage);
router.get('/groups/all', isAuth, adminOrTeacher, adminController.getGroups);
router.get('/groups/forLoggedInUser', isAuth, adminOrTeacher, adminController.getLoggedInUserGroups);
router.get('/group/:groupId', isAuth, adminOrTeacher, adminController.getGroup);
router.post('/groups/create', isAuth, adminOrTeacher, allowCrudForTeacher, [
    body(
        'groupName',
        'Meno musí obsahovať minimálne 3 znaky'
    )
        .isLength({min: 3})
        .trim()
        .custom((value, {req}) => {
            return Group.findOne({ where: { name: value } }).then(group => {
                if (group) {
                    return Promise.reject(
                        'Skupina s daným menom už existuje.'
                    );
                }
            });
        }),
], adminController.postCreateGroup);
router.post('/groups/update', isAuth, adminOrTeacher, allowCrudForTeacher, [
    body(
        'groupName',
        'Meno musí obsahovať minimálne 3 znaky'
    )
        .isLength({min: 3})
        .trim(),
], adminController.postEditGroup);
router.post('/group/delete', isAuth, adminOrTeacher, allowCrudForTeacher, adminController.postDeleteGroup);

router.get('/students', isAuth, adminOrTeacher, adminController.getStudentsPage);
router.get('/students/all', isAuth, adminOrTeacher, adminController.getStudents);
router.get('/students/forLoggedInUser', isAuth, adminOrTeacher, adminController.getLoggedInUserStudents);
router.get('/student/:userId', isAuth, adminOrTeacher, allowCrudForTeacher, adminController.getUser);
router.post('/students/create', isAuth, adminOrTeacher, allowCrudForTeacher, [
    check('emailAddress')
        .isEmail()
        .withMessage('Prosím zadajte validný e-mail.')
        .custom((value, {req}) => {
            return User.findOne({ where: { email: value } }).then(userDoc => {
                if (userDoc) {
                    return Promise.reject(
                        'E-mailová adresa už existuje. Prosím vyberte inú e-mailovú adresu.'
                    );
                }
            });
        })
        .normalizeEmail(),
    body(
        'firstName',
        'Meno musí obsahovať minimálne 3 znaky'
    )
        .isLength({min: 3})
        .trim(),
    body(
        'lastName',
        'Priezvisko musí obsahovať minimálne 3 znaky'
    )
        .isLength({min: 3})
        .trim(),
], adminController.postCreateUser);

router.post('/students/update', isAuth, adminOrTeacher, allowCrudForTeacher, [
    check('emailAddress')
        .isEmail()
        .withMessage('Prosím zadajte validný emmail.')
        .custom((value, {req}) => {
            return User.findOne({ where: { email: value, id: {[Op.notIn]:[req.body.userId]} } }).then(userDoc => {
                if (userDoc) {
                    return Promise.reject(
                        'E-mailová adresa už existuje. Prosím vyberte inú e-mailovú adresu.'
                    );
                }
            });
        })
        .normalizeEmail(),
    body(
        'firstName',
        'Meno musí obsahovať minimálne 3 znaky'
    )
        .isLength({min: 3})
        .trim(),
    body(
        'lastName',
        'Priezvisko musí obsahovať minimálne 3 znaky'
    )
        .isLength({min: 3})
        .trim(),
], adminController.postEditUser);
router.post('/student/delete', isAuth, adminOrTeacher, allowCrudForTeacher, adminController.postDeleteUser);
router.get('/task/:taskId', isAuth, adminOrTeacher, adminController.getTask);
router.post('/task/delete', isAuth, adminOrTeacher, allowCrudForTeacher, adminController.postDeleteTask);
router.get('/tasks/new', isAuth, adminOrTeacher, adminController.getTasksPage);
router.get('/tasks/index', isAuth, adminOrTeacher, adminController.getTasksIndexPage);
router.get('/tasks/get', isAuth, adminOrTeacher, adminController.getShowTasks);
router.post('/tasks/create', isAuth, adminOrTeacher, allowCrudForTeacher, [
    body(
        'title',
        'Meno musí obsahovať minimálne 3 znaky'
    )
        .isLength({min: 3})
        .trim(),
    body(
        'startedAt',
        'Začiatok je povinný údaj'
    )
        .not()
        .isEmpty()
        .trim(),
    body(
        'finishedAt',
        'Koniec je povinný údaj'
    )
        .not()
        .isEmpty()
        .trim(),
], adminController.postCreateTask);
router.post('/tasks/update', isAuth, adminOrTeacher, allowCrudForTeacher, [
    body(
        'title',
        'Meno musí obsahovať minimálne 3 znaky'
    )
        .isLength({min: 3})
        .trim(),
    body(
        'startedAt',
        'Začiatok je povinný údaj'
    )
        .not()
        .isEmpty()
        .trim(),
    body(
        'finishedAt',
        'Koniec je povinný údaj'
    )
        .not()
        .isEmpty()
        .trim(),
], adminController.postEditTask);
router.get('/games/index', isAuth, adminOrTeacher, adminController.getGamesPage);
router.get('/game/:gameId', isAuth, adminOrTeacher, adminController.getGame);
router.post('/game/delete', isAuth, adminOrTeacher, allowCrudForTeacher, adminController.postDeleteGame);
router.get('/games/all', isAuth, adminOrTeacher, adminController.getShowGames);
router.get('/games/forLoggedInUser', isAuth, adminOrTeacher, adminController.getLoggedInUserGames);
router.post('/games/create', isAuth, adminOrTeacher, allowCrudForTeacher, [
    body(
        'word',
        'Meno musí obsahovať minimálne 3 znaky'
    )
        .isLength({min: 3})
        .trim(),
    body(
        'row',
        'Meno musí obsahovať minimálne 3 znaky'
    )
        .isNumeric()
        .trim(),
    body(
        'col',
        'Meno musí obsahovať minimálne 3 znaky'
    )
        .isNumeric()
        .trim(),
], adminController.postCreateGame);

router.post('/games/update', isAuth, adminOrTeacher, allowCrudForTeacher, [
    body(
        'word',
        'Meno musí obsahovať minimálne 3 znaky'
    )
        .isLength({min: 3})
        .trim(),
    body(
        'row',
        'Meno musí obsahovať minimálne 3 znaky'
    )
        .isNumeric()
        .trim(),
    body(
        'col',
        'Meno musí obsahovať minimálne 3 znaky'
    )
        .isNumeric()
        .trim(),
], adminController.postEditGame);

router.get('/teachers', isAuth, isAdmin, adminController.getTeachersPage);
router.get('/teachers/all', isAuth, isAdmin, adminController.getTeachers);
router.get('/teacher/:userId', isAuth, isAdmin, adminController.getUser);
router.get('/teachers/forAdmin', isAuth, isAdmin, adminController.getTeachersForAdmin);

router.post('/teachers/create', isAuth, isAdmin, [
    check('emailAddress')
        .isEmail()
        .withMessage('Prosím zadajte validný e-mail.')
        .custom((value, {req}) => {
            return User.findOne({ where: { email: value } }).then(userDoc => {
                if (userDoc) {
                    return Promise.reject(
                        'E-mailová adresa uz existuje. Prosím vyberte inú e-mailovú adresu.'
                    );
                }
            });
        })
        .normalizeEmail(),
    body(
        'firstName',
        'Meno musí obsahovať minimálne 3 znaky'
    )
        .isLength({min: 3})
        .trim(),
    body(
        'lastName',
        'Priezvisko musí obsahovať minimálne 3 znaky'
    )
        .isLength({min: 3})
        .trim(),
], adminController.postCreateUser);

router.post('/teachers/update', isAuth, isAdmin, [
    check('emailAddress')
        .isEmail()
        .withMessage('Prosím zadajte validný e-mail.')
        .custom((value, {req}) => {
            return User.findOne({ where: { email: value, id: {[Op.notIn]:[req.body.userId]} } }).then(userDoc => {
                if (userDoc) {
                    return Promise.reject(
                        'E-mailová adresa uz existuje. Prosím vyberte inú e-mailovú adresu.'
                    );
                }
            });
        })
        .normalizeEmail(),
    body(
        'firstName',
        'Meno musí obsahovať minimálne 3 znaky'
    )
        .isLength({min: 3})
        .trim(),
    body(
        'lastName',
        'Priezvisko musí obsahovať minimálne 3 znaky'
    )
        .isLength({min: 3})
        .trim(),
], adminController.postEditUser);

router.post('/teacher/delete', isAuth, isAdmin, adminController.postDeleteUser);

module.exports = router;
