let swiper;

const getGroupsWithTasks = () => {
    const csrf = document.getElementById('csrf').value;

    fetch('/groups-with-tasks', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        }
    })
        .then(result => {
            return result.json();
        })
        .then(data => {
            if (data.responseOk) {
                const prevButton = document.getElementById('prevGroup');
                const nextButton = document.getElementById('nextGroup');
                swiper = new Swiper(data.groupsWithTasks);

                prevButton.addEventListener('click', () => swiper.swipe(false));
                nextButton.addEventListener('click', () => swiper.swipe());

                swiper.render();
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}

function Swiper(groups){
    this.actualPage = 0;
    this.maxPage = groups.length;
    this.groups = groups;
    this.group = null;
}

Swiper.prototype = {
    setGroupName: function (){
        document.getElementById('groupName').innerHTML = this.group.name;
    },

    swipe: function (next=true){
        if ((this.actualPage === 0 && !next) || (next && this.maxPage - 1 === this.actualPage)){
            return;
        }

        next ? this.actualPage++ : this.actualPage--;

        this.render();
    },

    renderTasks: function (){
        const taskElem = document.getElementById('tasks');
        taskElem.innerHTML = "";
        const actualDate = new Date();
        const old = this.group.tasks.filter(task => new Date(task.to) < actualDate);
        const future = this.group.tasks.filter(task => new Date(task.from) > actualDate);
        const actual = this.group.tasks.filter(task => new Date(task.from) <= actualDate && new Date(task.to) >= actualDate).sort((a,b) => {
            if (new Date(a.to) < new Date(b.to)){
                return -1;
            }
            return 1;
        });
        [...actual, ...future, ...old].map(task => taskElem.appendChild(taskCard(task, this.group, true)));
    },

    setGroup: function (){
        this.group = this.groups[this.actualPage];
    },

    render: function (){
        this.setDisabledBtns();
        this.setGroup();
        this.setGroupName();
        this.renderTasks();
    },

    setDisabledBtns: function (){
        if (this.actualPage === 0){
            document.getElementById('prevGroup').classList.add('swiper-button-disabled');
        }else{
            document.getElementById('prevGroup').classList.remove('swiper-button-disabled');
        }

        if (this.actualPage === this.maxPage - 1){
            document.getElementById('nextGroup').classList.add('swiper-button-disabled');
        }
        else{
            document.getElementById('nextGroup').classList.remove('swiper-button-disabled');
        }
    }
}

getGroupsWithTasks();