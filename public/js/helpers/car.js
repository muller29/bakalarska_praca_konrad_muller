const car = "/img/car.png";
const directionReverse = { "right": "left", "left": "right", "up": "down", "down": "up" };

function Car() {
    this.id = "car";
    this.src = car;
    this.r = 0;
    this.c = 0;
    this.angle = 0;
    this.elem = null;
    this.direction = null;
    this.reqID = null;
    this.running = true;
    this.lastRoad = null;
    this.roadsToMove = [];
    this.coefficient = 25; // other curves
    this.coefficient2 = 12; // for wave curve
    this.count = 0;
    this.animationFinish = false;
}

Car.prototype = {
    constructor: Car,

    resetCar: function (r, c, dir, lastRoad){
        this.r = r;
        this.c = c;
        this.direction = dir;
        this.lastRoad = lastRoad;
        this.setInitialAngle();
    },

    render: function () {
        const parent = document.querySelectorAll(`[data-row="${this.r}"][data-col="${this.c}"]`)[0];
        let image;
        if (this.elem && document.getElementById('car')){
            const car = document.getElementById('car');
            this.parent = parent;
            this.elem = car;
            car.parentElement.removeChild(car);
            parent.appendChild(car);
            this.setLastRoad(parent.querySelector('img.roads'));
            car.style.left = "15px";
            car.style.top = "7px";
            car.style.webkitTransform = `rotate(${this.angle}deg)`;
            car.style.mozTransform = `rotate(${this.angle}deg)`;
            car.style.msTransform = `rotate(${this.angle}deg)`;
            car.style.oTransform = `rotate(${this.angle}deg)`;
            car.style.transform = `rotate(${this.angle}deg)`;

        }else{
            image = document.createElement("IMG");
            image.setAttribute('class', "car-icon");
            image.setAttribute('draggable', "false");
            image.setAttribute("src", this.src);
            image.setAttribute("id", this.id);
            parent.appendChild(image);
            this.setLastRoad(parent.querySelector('img.roads'));
            this.parent = parent;
            this.elem = image;
            this.rotateBy(this.angle);
        }
    },

    computeAngle: function () {
        const st = window.getComputedStyle(this.elem, null);
        const tr = st.getPropertyValue("-webkit-transform") || st.getPropertyValue("-moz-transform") || st.getPropertyValue("-ms-transform") || st.getPropertyValue("-o-transform") || st.getPropertyValue("transform")

        const values = tr.split('(')[1].split(')')[0].split(',');
        const [a, b] = values;
        return Math.round((Math.atan2(b, a) * (180 / Math.PI)) * 1000) / 1000;
    },

    animate: function (x, y, step, rotateStep, rotateStep2, callback) {
        if (y === 0) {
            this.elem.style.left = `${this.elem.offsetLeft + step}px`;
        } else if (x === 0) {
            this.elem.style.top = `${this.elem.offsetTop + step}px`;
        }

        const angle = this.computeAngle();
        const degree = rotateStep2 === 0 ? angle + rotateStep : this.count < 12 ? angle + rotateStep2 : this.count === 12 ? angle : angle + rotateStep;

        this.rotateBy(degree);
        this.setAngle(degree);

        if (this.count < this.coefficient - 1) {
            this.count++;
            requestAnimationFrame(() => {
                this.animate(x, y, step, rotateStep, rotateStep2, callback);
            });
        } else {
            if (!_.isEmpty(this.roadsToMove)) {
                this.moveCar(callback);
            }else{
                if (callback) {
                    callback(true);
                }
            }
        }
    },

    moveCar: function (callback= null) {
        this.setAnimationFinish(false);
        this.count = 0;
        const road = this.roadsToMove.shift();
        if (road === undefined){return};
        const imgElem = road.querySelector('img');
        const roadType = imgElem.getAttribute('data-name');
        const [x, y, rotate, rotate2] = this.calculateMovement(roadType);

        const [step, rotateStep, rotateStep2] = this.computeSteps(x, y, rotate, rotate2);

        this.setLastRoad(imgElem);
        this.setPosition(parseInt(road.getAttribute('data-row'), 10), parseInt(road.getAttribute('data-col'), 10));

        this.animate(x, y, step, rotateStep, rotateStep2, callback);
        if (callback){
            callback(false);
        }
    },

    computeSteps: function (x, y, rotate, rotate2) {
        const coef = rotate2 === 0 ? this.coefficient : this.coefficient2;
        const step = x === 0 ? y / this.coefficient : x / this.coefficient;
        const rotateStep = rotate / coef;
        const rotateStep2 = rotate2 / coef;

        return [step, rotateStep, rotateStep2];
    },

    rotateBy: function (degree){
        this.elem.style.webkitTransform = `rotate(${degree}deg)`;
        this.elem.style.mozTransform = `rotate(${degree}deg)`;
        this.elem.style.msTransform = `rotate(${degree}deg)`;
        this.elem.style.oTransform = `rotate(${degree}deg)`;
        this.elem.style.transform = `rotate(${degree}deg)`;
    },

    setDirection: function (dir) {
        this.direction = dir;
    },

    setPosition: function (i, j) {
        this.r = i;
        this.c = j;
    },

    setAngle: function (angle) {
      this.angle = angle;
    },

    calculateMovement: function (roadType) {
        switch (roadType) {
            case "horizontalRoad":
                const x = this.direction === "right" ? 50 : -50;
                let rotateH = 0;

                if (['upRightRoad', 'downLeftRoad'].includes(this.lastRoad.getAttribute('data-name'))) {
                    rotateH = 45;
                }
                if (['upLeftRoad', 'downRightRoad'].includes(this.lastRoad.getAttribute('data-name'))) {
                    rotateH = -45;
                }

                return [x, 0, rotateH, 0];

            case "verticalRoad":
                const y = this.direction === "down" ? 50 : -50;
                let rotateV = 0;
                if (['upRightRoad', 'downLeftRoad'].includes(this.lastRoad.getAttribute('data-name'))) {
                    rotateV = -45;
                }
                if (['upLeftRoad', 'downRightRoad'].includes(this.lastRoad.getAttribute('data-name'))) {
                    rotateV = 45;
                }
                return [0, y, rotateV, 0];

            case "upLeftRoad":
                const xUL = this.direction === "right" ? 50 : 0;
                const yUL = this.direction === "up" ? -50 : 0;
                let rotateUL = this.direction === "right" ? 45 : -45;
                let rotateUL2 = 0;

                if (this.lastRoad.getAttribute('data-name') === 'upRightRoad') {
                    rotateUL += 45;
                } else if (this.lastRoad.getAttribute('data-name') === 'downLeftRoad') {
                    rotateUL -= 45;
                } else if (this.direction === "right" && this.lastRoad.getAttribute('data-name') === 'downRightRoad') {
                    rotateUL2 -= 45;
                } else if (this.direction === "up" && this.lastRoad.getAttribute('data-name') === 'downRightRoad') {
                    rotateUL2 += 45;
                }

                this.direction === "up" ? this.setDirection("left") : this.setDirection("down");

                return [xUL, yUL, rotateUL, rotateUL2];

            case "upRightRoad":
                const xUR = this.direction === "left" ? -50 : 0;
                const yUR = this.direction === "up" ? -50 : 0;
                let rotateUR = this.direction === "left" ? -45 : 45;
                let rotateUR2 = 0;

                if (this.lastRoad.getAttribute('data-name') === 'upLeftRoad') {
                    rotateUR -= 45;
                } else if (this.lastRoad.getAttribute('data-name') === 'downRightRoad') {
                    rotateUR += 45;
                } else if (this.direction === "left" && this.lastRoad.getAttribute('data-name') === 'downLeftRoad') {
                    rotateUR2 += 45;
                } else if (this.direction === "up" && this.lastRoad.getAttribute('data-name') === 'downLeftRoad') {
                    rotateUR2 -= 45;
                }

                this.direction === "up" ? this.setDirection("right") : this.setDirection("down");

                return [xUR, yUR, rotateUR, rotateUR2];

            case "downLeftRoad":
                const xDL = this.direction === "right" ? 50 : 0;
                const yDL = this.direction === "down" ? 50 : 0;
                let rotateDL = this.direction === "right" ? -45 : 45;
                let rotateDL2 = 0;

                if (this.lastRoad.getAttribute('data-name') === 'upLeftRoad') {
                    rotateDL += 45;
                } else if (this.lastRoad.getAttribute('data-name') === 'downRightRoad') {
                    rotateDL -= 45;
                } else if (this.direction === "down" && this.lastRoad.getAttribute('data-name') === 'upRightRoad') {
                    rotateDL2 -= 45;
                } else if (this.direction === "right" && this.lastRoad.getAttribute('data-name') === 'upRightRoad') {
                    rotateDL2 += 45;
                }

                this.direction === "down" ? this.setDirection("left") : this.setDirection("up");

                return [xDL, yDL, rotateDL, rotateDL2];

            case "downRightRoad":
                const xDR = this.direction === "left" ? -50 : 0;
                const yDR = this.direction === "down" ? 50 : 0;
                let rotateDR = this.direction === "left" ? 45 : -45;
                let rotateDR2 = 0;

                if (this.lastRoad.getAttribute('data-name') === 'upRightRoad') {
                    rotateDR -= 45;
                } else if (this.lastRoad.getAttribute('data-name') === 'downLeftRoad') {
                    rotateDR += 45;
                } else if (this.direction === "down" && this.lastRoad.getAttribute('data-name') === 'upLeftRoad') {
                    rotateDR2 += 45;
                } else if (this.direction === "left" && this.lastRoad.getAttribute('data-name') === 'upLeftRoad') {
                    rotateDR2 -= 45;
                }

                this.direction === "down" ? this.setDirection("right") : this.setDirection("up");

                return [xDR, yDR, rotateDR, rotateDR2];
            default:
                return [0, 0, 0];
        }
    },

    setInitialAngle: function (){
      switch (this.direction){
          case "right":
              this.angle = 90;
              break;
          case "left":
              this.angle = -90;
              break;
          case "down":
              this.angle = 180;
              break;
          default:
              this.angle = 0;
              break;
      }
    },

    setRoads: function (roads) {
        this.roadsToMove = roads;
    },

    setLastRoad: function (lastRoad) {
        this.lastRoad = lastRoad;
    },

    setAnimationFinish: function (bool) {
        this.animationFinish = bool;
    },

    setLastWasReversalMove: function (bool){
        this.lastWasReversalMove = bool;
    }
}