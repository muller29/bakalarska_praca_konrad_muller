function CountDownTimer(duration, granularity, elem) {
    this.elem = elem;
    this.duration = duration;
    this.granularity = granularity || 1000;
    this.tickFtns = [];
    this.running = false;
}

CountDownTimer.prototype.start = function() {
    if (this.running) {
        return;
    }
    this.running = true;
    let start = Date.now(),
        that = this,
        diff, obj;

    (function timer() {
        diff = that.duration - (((Date.now() - start) / 1000) | 0);

        if (diff > 0) {
            setTimeout(timer, that.granularity);
        } else {
            diff = 0;
            that.running = false;
        }

        obj = CountDownTimer.parse(diff);
        that.elem.innerHTML = `${obj.days}d ${obj.hours}h ${obj.minutes}m ${obj.seconds}s`
        that.tickFtns.forEach(function(ftn) {
            ftn.call(this, obj.minutes, obj.seconds);
        }, that);
    }());
};

CountDownTimer.prototype.onTick = function(ftn) {
    if (typeof ftn === 'function') {
        this.tickFtns.push(ftn);
    }
    return this;
};

CountDownTimer.prototype.expired = function() {
    return !this.running;
};

CountDownTimer.parse = function(seconds) {
    const days = Math.floor(seconds / 60 / 60 / 24);
    const hours = Math.floor(seconds / 3600 % 24);
    const minutes = Math.floor(seconds / 60 % 60);
    const secs = Math.floor(seconds % 60);
    return {
        'days': days | 0,
        'hours': hours | 0,
        'minutes': minutes | 0,
        'seconds': secs | 0
    };
};