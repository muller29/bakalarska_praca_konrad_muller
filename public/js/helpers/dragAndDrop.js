const directionPairs = {
    "right": [0, 1],
    "left": [0, -1],
    "up": [-1, 0],
    "down": [1, 0]
}

const roadPairs = (direction) => {
    switch (direction) {
        case "right":
            return {"horizontalRoad": [0, 1], "downLeftRoad": [-1, 0], "upLeftRoad": [1, 0]};
        case "left":
            return {"horizontalRoad": [0, -1], "upRightRoad": [1, 0], "downRightRoad": [-1, 0]};
        case "up":
            return {"verticalRoad": [-1, 0], "upLeftRoad": [0, -1], "upRightRoad": [0, 1]};
        case "down":
            return {"verticalRoad": [1, 0], "downLeftRoad": [0, -1], "downRightRoad": [0, 1]};
        default:
            return [];
    }
}

const nextDirection = (lastDir, road) => {
    switch (lastDir) {
        case "right":
            if (road === "horizontalRoad") {
                return "right";
            } else if (road === "downLeftRoad") {
                return "up";
            } else if (road === "upLeftRoad") {
                return "down";
            } else {
                return "not1";
            }
        case "left":
            if (road === "horizontalRoad") {
                return "left";
            } else if (road === "upRightRoad") {
                return "down";
            } else if (road === "downRightRoad") {
                return "up";
            } else {
                return "not2";
            }
        case "up":
            if (road === "verticalRoad") {
                return "up";
            } else if (road === "upLeftRoad") {
                return "left";
            } else if (road === "upRightRoad") {
                return "right";
            } else {
                return "not3";
            }
        case "down":
            if (road === "verticalRoad") {
                return "down";
            } else if (road === "downLeftRoad") {
                return "left";
            } else if (road === "downRightRoad") {
                return "right";
            } else {
                return "not4";
            }

        default:
            return null;
    }
}

function DragAndDrop() {
}

DragAndDrop.prototype = {
    constructor: DragAndDrop,

    onDragStart: function (event) {
        event.dataTransfer.clearData();
        const id = event.target.getAttribute('id');

        event.dataTransfer.effectAllowed = 'move';
        event.dataTransfer.setData('text', id);
    },

    onDragOver: function (event) {
        event.preventDefault();
        return false;
    },

    onDrop: function (event, imgCount) {
        event.stopPropagation();
        const id = event.dataTransfer.getData("text");
        const draggableElement = document.getElementById(id);
        const dropzone = event.currentTarget;
        const elementInPlayzone = draggableElement.getAttribute('data-used');
        const isLastRoad = task.actualGame.lastRoad.querySelector('img').getAttribute('id') === id;

        if(elementInPlayzone && isLastRoad && dropzone.classList.contains('fa-trash')){
            const wrapper = draggableElement.parentNode;
            const cell = wrapper.parentNode;
            const r = parseInt(cell.getAttribute('data-row'), 10);
            const c = parseInt(cell.getAttribute('data-col'), 10);
            const deletedRoad = task.actualGame.roads.pop();
            const dir = task.actualGame.game.level === "easy" ? task.actualGame.calculateDefaultDirection(0, 0) : task.actualGame.calculateDefaultDirectionOnMedium();
            const lastPos = task.actualGame.roads.length === 0 ? [task.actualGame.sR, task.actualGame.sC, dir] : task.actualGame.roads[task.actualGame.roads.length-1];

            const lastRoadElem = document.querySelectorAll(`[data-row="${lastPos[0]}"][data-col="${lastPos[1]}"]`)[0];
            task.actualGame.setRoadPosition(r, c, null);
            task.actualGame.setLastRoad(lastRoadElem);
            task.actualGame.setLastPos(...lastPos);
            task.actualGame.setDirection(nextDirection(lastPos[2], lastRoadElem.querySelector('img').getAttribute('data-name')));
            cell.removeChild(wrapper);

            if (task.actualGame.car.r === r && task.actualGame.car.c === c) {
                task.actualGame.car.resetCar(task.actualGame.sR, task.actualGame.sC, dir, 90, lastRoadElem);
                task.actualGame.car.render();
                task.actualGame.reversalMove(cell);
            }
        }
        else if (DragAndDrop.isAllowedToPut(dropzone, draggableElement) && !elementInPlayzone && !isLastRoad) {
            let tag = draggableElement.getAttribute("data-tag");
            const index = parseInt(tag.slice(-1)) - 1;
            const imgRow = imgDict[index];
            const img = new Image(imgRow.url, imgRow.tag, imgRow.name, imgCount);
            const wrapper = img.createImg();

            draggableElement.parentElement.replaceWith(wrapper);
            dropzone.appendChild(draggableElement.parentElement);
            draggableElement.setAttribute("data-used", "true");

            const direction = task.actualGame.getDirection();
            const r = parseInt(dropzone.getAttribute('data-row'), 10);
            const c = parseInt(dropzone.getAttribute('data-col'), 10);
            task.actualGame.addRoad([r, c, direction]);

            // put letter to Text
            if (DragAndDrop.isLetter(dropzone.childNodes)) {
                task.actualGame.setActualWord(DragAndDrop.getLetter(dropzone.childNodes).innerHTML);
            }

            task.actualGame.setRoadPosition(r, c, imgRow.name);
            task.actualGame.increaseImgCount();
            task.actualGame.setLastRoad(dropzone);
            task.actualGame.setLastPos(r, c);
            task.actualGame.setDirection(nextDirection(direction, draggableElement.getAttribute('data-name')));

            task.actualGame.checkGameCompletion(r, c);
        }

        return false;
    },
}


DragAndDrop.isLetter = function (elements) {
    return [...elements].filter(el => el.classList.contains('absolute-letter')).length > 0;
}

DragAndDrop.getLetter = function (elements) {
    return [...elements].filter(el => el.classList.contains('absolute-letter'))[0];
}

DragAndDrop.onlyLetters = function (elements) {
    return [...elements].map(el => el.classList.contains('absolute-letter')).every(Boolean);
}

DragAndDrop.isAllowedToPut = function (actualRoad, imgToPut) {
    const lastRoad = task.actualGame.getLastRoad();
    const actualRoadRow = parseInt(actualRoad.getAttribute('data-row'), 10);
    const actualRoadCol = parseInt(actualRoad.getAttribute('data-col'), 10);
    const lastRoadRow = parseInt(lastRoad.getAttribute('data-row'), 10);
    const lastRoadCol = parseInt(lastRoad.getAttribute('data-col'), 10);
    const direction = task.actualGame.getDirection();
    const roadOptions = roadPairs(direction);
    const nextOptions = Object.keys(roadOptions).map(dir => {
        const nextDirection = roadOptions[dir];
        const nextPosition = [actualRoadRow + nextDirection[0], actualRoadCol + nextDirection[1]];
        const outOfRange = task.actualGame.isOutOfRange(...nextPosition);
        if (outOfRange && (!actualRoad.classList.contains('finishLH') && !actualRoad.classList.contains('finishRH') && !actualRoad.classList.contains('finishUV') && !actualRoad.classList.contains('finishDV'))) {
            return null;
        }
        if (actualRoad.classList.contains('finishLH') || actualRoad.classList.contains('finishRH') || actualRoad.classList.contains('finishUV') || actualRoad.classList.contains('finishDV')){
            if (actualRoadCol === 0 && ["upLeftRoad", "downLeftRoad", "horizontalRoad"].includes(dir)){
                return dir;
            }
            else if (actualRoadCol === task.actualGame.c - 1 && ["downRightRoad", "upRightRoad", "horizontalRoad"].includes(dir)){
                return dir;
            }
            else if (actualRoadRow === 0 && ["downLeftRoad", "downRightRoad", "verticalRoad"].includes(dir)){
                return dir;
            }else if(actualRoadRow === task.actualGame.r - 1 && ["upRightRoad", "upLeftRoad", "verticalRoad"].includes(dir)){
                return dir;
            }
            return null;
        }
        return dir;
    }).filter(el => el !== null);
    const isNextAllowed = nextOptions.includes(imgToPut.getAttribute('data-name'));

    // Control the road is next to the last Road
    const supposedDirection = directionPairs[direction];
    const supposedPosition = [supposedDirection[0] + lastRoadRow, supposedDirection[1] + lastRoadCol];

    const isAfterTheLastRoad = [supposedPosition[0] === actualRoadRow, supposedPosition[1] === actualRoadCol].every(Boolean);

    return (actualRoad.childElementCount === 0 || DragAndDrop.onlyLetters(actualRoad.childNodes)) && isNextAllowed && isAfterTheLastRoad;
}