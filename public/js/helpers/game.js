const road1Img = "/img/road1.png";
const road2Img = "/img/road2.png";
const road3Img = "/img/road3.png";
const road4Img = "/img/road4.png";
const road5Img = "/img/road5.png";
const road6Img = "/img/road6.png";

const imgDict = [
    {url: road1Img, tag: "road1", name: "verticalRoad"},
    {url: road2Img, tag: "road2", name: "horizontalRoad"},
    {url: road3Img, tag: "road3", name: "upLeftRoad"},
    {url: road4Img, tag: "road4", name: "upRightRoad"},
    {url: road5Img, tag: "road5", name: "downRightRoad"},
    {url: road6Img, tag: "road6", name: "downLeftRoad"},
];

const getImgByName = (name) => {
    switch (name) {
        case "verticalRoad":
            return imgDict[0];
        case "horizontalRoad":
            return imgDict[1];
        case "upLeftRoad":
            return imgDict[2];
        case "upRightRoad":
            return imgDict[3];
        case "downRightRoad":
            return imgDict[4];
        case "downLeftRoad":
            return imgDict[5];
        default:
            return imgDict[0];
    }
}

if(window.Prototype) {
    delete Object.prototype.toJSON;
    delete Array.prototype.toJSON;
    delete Hash.prototype.toJSON;
    delete String.prototype.toJSON;
}

function Game(game, userGame) {
    this.searchedWordElem = null;
    this.actualWordElem = null;
    this.playground = null;
    this.tempPlayground = null;
    this.game = game;
    this.userGame = userGame;
    this.twoletters = ['dz', 'ch', 'dž'];
    this.lettersToChars = { "dž": '*', 'dz': '?', 'ch': '#' };
    this.charsToLetter = { "*": "dž", '?': 'dz', '#': 'ch' };
    this.word = game.word;
    this.r = parseInt(game.rows, 10);
    this.c = parseInt(game.columns, 10);
    this.sR = 0;
    this.sC = 0;
    this.fR = 0;
    this.fC = 0;
    this.imgCount = 1;
    this.roads = [];
    this.lastRoadPos = [0, 0];
    this.lastRoad = null;
    this.direction = null;
    this.saveInterval = null;
    this.startClass = "";
    this.finishClass = "";
    this.actualWord = "";
    this.emptyCellPositions = [];
    this.car = new Car();
    this.car.setLastRoad();
}

extend(DragAndDrop, Game);

Game.prototype = {
    constructor: Game,

    scatterWord: function (){
        switch (this.game.level){
            case "easy":
                this.createEasyGame();
                break;
            case "medium":
            case "hard":
                this.createMediumGame();
                break;
            default:
                this.createEasyGame();
                break;
        }
    },

    initialize: function (){
        this.writeWordIntoScreen();
        this.renderPlayGround();
        this.readAndRenderImages();
        this.runSaving();
    },

    initializeCar: function (){
        this.car.setInitialAngle();
        this.car.render();
    },

    createMediumGame: function (){
      this.generateDefaultPlayground();
      this.placeStartAndFinish();
      this.placeEmptyCells();
      this.findShortestPath();
    },

    generateDefaultPlayground: function (){
        this.playground2D = new Array(this.r).fill(null).map(item => (new Array(this.c).fill(null)));
    },

    placeStartAndFinish: function (){
      const rowMin = new Array(this.c).fill(null).map((item, i) => [0, i]);
      const rowMax = new Array(this.c).fill(null).map((item, i) => [this.r-1, i]);
      const colMin = new Array(this.r).fill(null).map((item, i) => [i, 0]);
      const colMax = new Array(this.r).fill(null).map((item, i) => [i, this.c-1]);

      const indexes = _.shuffle([...rowMin, ...rowMax, ...colMin, ...colMax]);

      const [sR, sC] = _.sample(indexes);
      this.sR = sR;
      this.sC = sC;
      let [fR, fC] = _.sample(indexes);
      while(sR === fR && sC === fC){
          [fR, fC] = _.sample(indexes);
      }

      this.fR = fR;
      this.fC = fC;

      const colMinFiltered = colMin.filter(pos => pos[0] === sR && pos[1] === sC);
      const colMaxFiltered = colMax.filter(pos => pos[0] === sR && pos[1] === sC);

      if (colMinFiltered.length > 0 || colMaxFiltered.length > 0){
          this.playground2D[sR][sC] = "S|horizontalRoad";
          this.startClass = sC === 0 ? "startLH" : "startRH";
      }else{
          this.playground2D[sR][sC] = "S|verticalRoad";
          this.startClass = sR === 0 ? "startUV" : "startDV";
      }

      if (fC === 0 || fC === this.c - 1) {
          this.finishClass = fC === 0 ? "finishLH" : "finishRH";
      }else{
          this.finishClass = fR === 0 ? "finishUV" : "finishDV";
      }

      this.playground2D[fR][fC] = 'F';
      this.tempPlayground = _.clone(this.playground2D);
    },

    placeEmptyCells: function (){
        const indexes = new Array(this.r).fill(null).map((item, i) => (new Array(this.c).fill(null).map((it, j) => [i, j])));
        let selectedInd = indexes.map(pos => pos.filter(posJ => !((posJ[0] === this.sR && posJ[1] === this.sC) || (posJ[0] === this.fR && posJ[1] === this.fC))));

        let count = 0;
        while(count < this.word.length){
            const rowSelection = _.sample(selectedInd);
            const pairSelection = _.sample(rowSelection);
            const [r, c] = pairSelection;
            if (this.checkTempPlaygroundEmpty(r, c)){
                this.tempPlayground[r][c] = "x";
                this.emptyCellPositions.push([r,c]);
                selectedInd = selectedInd.map(pos => pos.filter(posJ => !(posJ[0] === r && posJ[1] === c)));
                count++;
            }
        }
    },

    findShortestPath: function (){
        const graph = new Graph(this.tempPlayground);
        let start = graph.grid[this.sR][this.sC];
        const bcktr = new Backtracking(graph, this.word.length);
        const result = bcktr.start(start);

        const splittedWord = this.word.replace('dz', '?').replace('dž', '*').replace('ch', '#');
        let letterCounter = 0;
        result.forEach((node) => {
            if (node.letter === "x"){
                const selectedLetter = splittedWord[letterCounter];
                this.playground2D[node.x][node.y] = ['?', '*', '#'].includes(selectedLetter) ? this.charsToLetter[selectedLetter] : selectedLetter;
                letterCounter++;
            }
        });
    },

    checkTempPlaygroundEmpty: function (r, c){
        const directions = {"up": [r - 1, c], "down": [r + 1, c], "left": [r, c - 1], "right": [r, c + 1]};
        const directionBools = Object.keys(directions).map(key => this.isOutOfRange(...directions[key]) || this.isEmptyTempIndex(...directions[key]));
        return directionBools.every(Boolean) && !['S', 'F'].includes(this.tempPlayground[r][c]);
    },

    createEasyGame: function () {
        const splittedWord = this.word.replace('dz', '?').replace('dž', '*').replace('ch', '#');
        let charIndex = 0;
        const startPole = [0, 0];
        const finishPole = [this.r - 1, this.c - 1];

        let letterPut = true;
        let actualFieldIndex = 0;
        const divider = (this.r * this.c - 2) / this.word.length;

        this.playground2D = new Array(this.r).fill(null).map(item => (new Array(this.c).fill(null)));

        this.playground2D[startPole[0]][startPole[1]] = "S|horizontalRoad";
        this.playground2D[finishPole[0]][finishPole[1]] = 'F';
        this.startClass = "startLH";
        this.finishClass = this.r % 2 === 0 ? "finishLH" : "finishRH";
        this.sR = 0;
        this.sC = 0;

        for (let i = 0; i < this.playground2D.length; i++) {
            for (let j = 0; j < this.playground2D[0].length; j++) {
                if (Math.floor((charIndex + 1) * divider) !== actualFieldIndex && letterPut) {
                    actualFieldIndex++;
                    continue;
                }

                if (this.isFreePosition(i, j) && charIndex < this.word.length) {
                    const selectedLetter = splittedWord[charIndex];
                    this.playground2D[i][j] = ['?', '*', '#'].includes(selectedLetter) ? this.charsToLetter[selectedLetter] : selectedLetter;
                    charIndex++;
                    letterPut = true;
                } else {
                    letterPut = false;
                    if (charIndex + 1 === this.word.length) {
                        const letters = this.playground2D[this.r-1].map((item, k) => item !== null && item.indexOf('F') === -1 ? k : null).filter(item => item !== null);
                        const letter = letters.length > 0 ? letters.pop() : 0;
                        const ignoreLetters = this.playground2D[this.r - 1].map((item, k) => item === null && k >= letter ? k : null);
                        const lastRowSamples = ignoreLetters.filter(item => item !== null && this.isFreePosition(this.r - 1, item));
                        const selectedLetter = splittedWord[charIndex];
                        this.playground2D[this.r - 1][_.sample(lastRowSamples)] = ['?', '*', '#'].includes(selectedLetter) ? this.charsToLetter[selectedLetter] : selectedLetter;
                        letterPut = true;
                    }
                }

                actualFieldIndex++;
            }

            if (i % 2 === 1) {
                this.playground2D[i] = this.playground2D[i].reverse()
            }
        }
    },

    renderPlayGround: function () {
        const gameElem = document.getElementById('game');
        gameElem.innerHTML = "";

        const trashWrapper = document.createElement('div');
        trashWrapper.classList.add('trash-wrapper');
        const icon = document.createElement('i');
        icon.classList.add("fa", "fa-trash");
        icon.setAttribute('aria-hidden', "true");
        icon.addEventListener("dragover", this.onDragOver);
        icon.addEventListener("drop", this.onDrop);
        trashWrapper.appendChild(icon);
        gameElem.appendChild(trashWrapper);
        for (let i = 0; i < this.r; i++) {
            let row = document.createElement("div");
            row.classList.add("row", "d-flex", "justify-content-center");
            for (let j = 0; j < this.c; j++) {
                let cell = document.createElement("div");
                cell.classList.add("grid-square");
                cell.id = `cell-${this.r * i + j + i}`;
                cell.setAttribute('data-row', i.toString());
                cell.setAttribute('data-col', j.toString());

                const arrValue = this.playground2D[i][j] ? this.playground2D[i][j].split('|') : null;
                if (arrValue) {
                    if (this.playground2D[i][j].indexOf("S") !== -1) {
                        cell.classList.add(this.startClass);

                        if (!this.lastRoad) {
                            this.lastRoad = cell;
                            this.setLastPos(i, j);
                        }
                        const dir = this.game.level === "easy" ? this.calculateDefaultDirection(i, j) : this.calculateDefaultDirectionOnMedium(i, j);
                        if (!this.direction) {
                            this.direction = dir;
                            this.car.setDirection(dir);
                            this.car.setPosition(i, j);
                        }
                    }

                    if (this.playground2D[i][j].indexOf("F") !== -1) {
                        cell.classList.add(this.finishClass);
                    }

                    if (!this.elementIsRoad(arrValue[0]) && !['S', 'F', null].includes(arrValue[0]) && arrValue[0].match(/\p{L}/u)) {
                        const letterElem = document.createElement("p");
                        letterElem.appendChild(document.createTextNode(arrValue[0]));
                        letterElem.classList.add('absolute-letter');
                        cell.appendChild(letterElem);
                    }

                    if (this.elementIsRoad(arrValue[arrValue.length - 1])) {
                        const road = arrValue[arrValue.length - 1];
                        const image = getImgByName(road);
                        let defaultImg;
                        if (this.playground2D[i][j].indexOf("S") !== -1) {
                            defaultImg = new Image(image.url, image.tag, image.name, this.imgCount, false);
                        }else{
                            defaultImg = new Image(image.url, image.tag, image.name, this.imgCount, true);
                        }
                        this.imgCount++;
                        const wrapper = defaultImg.createImg();
                        cell.appendChild(wrapper);
                    }
                }
                cell.addEventListener("dragover", this.onDragOver);
                cell.addEventListener("drop", this.onDrop);
                row.appendChild(cell);
            }
            gameElem.appendChild(row);
        }

        this.setInitialLastRoad();
    },

    setInitialLastRoad: function () {
        const r = !_.isEmpty(this.lastRoadPos) ? this.lastRoadPos[0] : 0;
        const c = !_.isEmpty(this.lastRoadPos) ? this.lastRoadPos[1] : 0;
        const last = document.querySelectorAll(`[data-row="${r}"][data-col="${c}"]`)[0];
        this.setLastRoad(last);
        this.car.setLastRoad(last.querySelector(".roads"));
    },

    elementIsRoad: function (elem) {
        return ["horizontalRoad", "verticalRoad", "upLeftRoad", "upRightRoad", "downLeftRoad", "downRightRoad"].includes(elem);
    },

    readAndRenderImages: function () {
        const commandsElem = document.getElementById('commands');
        const commandWrap = document.createElement('div');
        commandWrap.classList.add('d-flex', 'flex-wrap');
        if (commandsElem) {
            commandsElem.innerHTML = "";
            imgDict.map((key, i) => {
                const img = new Image(key.url, key.tag, key.name, this.imgCount);
                const wrapper = img.createImg();
                commandWrap.appendChild(wrapper);
                this.imgCount++;
                if (i % 2 === 0 && i !== 0 && i !== imgDict.length - 2) {
                    const newLine = document.createElement('div');
                    newLine.classList.add('w-100', 'm-2');
                    commandWrap.appendChild(newLine);
                }
            });
            commandsElem.appendChild(commandWrap);
        }
    },

    writeWordIntoScreen: function () {
        this.searchedWordElem = document.getElementById('searchedWord');
        if (!this.searchedWordElem){
            return;
        }
        this.searchedWordElem.innerHTML = this.word;
        const lastLetters = this.lastRoad ? [...this.actualWordElem.children] : [];
        this.actualWordElem = document.getElementById('actualWord');
        this.actualWordElem.innerHTML = "";
        [...this.word].forEach((letter, i) => {
            let letterSpan;
            if (i < lastLetters.length){
                letterSpan = lastLetters[i];
            }else{
                letterSpan = document.createElement('span');
                letterSpan.appendChild(document.createTextNode("_ "));
            }
            this.actualWordElem.appendChild(letterSpan);
        });
    },

    isFreePosition: function (r, c) {
        const directions = {"up": [r - 1, c], "down": [r + 1, c], "left": [r, c - 1], "right": [r, c + 1]};
        const directionBools = Object.keys(directions).map(key => this.isOutOfRange(...directions[key]) || this.isEmptyIndex(...directions[key]));
        return directionBools.every(Boolean) && !['S', 'F'].includes(this.playground2D[r][c]);
    },

    isInArray: function (r, c) {
        return r > -1 && r < this.r && c > -1 && c < this.c;
    },

    isOutOfRange: function (r, c) {
        return r < 0 || r >= this.r || c < 0 || c >= this.c;
    },

    isEmptyTempIndex: function (r, c){
        if (this.isInArray(r, c)) {
            return this.tempPlayground[r][c] === null;
        }
        return false;
    },

    isEmptyIndex: function (r, c) {
        if (this.isInArray(r, c)) {
            return this.playground2D[r][c] === null || ['S', 'F'].includes(this.playground2D[r][c]);
        }
        return false;
    },

    onDragOver: function (event) {
        DragAndDrop.prototype.onDragOver.call(this, event);
    },

    onDrop: function (event) {
        DragAndDrop.prototype.onDrop.call(this, event, task.actualGame.imgCount);
    },

    increaseImgCount: function () {
        this.imgCount++;
    },

    getLastRoad: function () {
        return this.lastRoad;
    },

    setLastRoad: function (lastElement) {
        this.lastRoad = lastElement;
    },

    setLastPos: function (lastPosX, lastPosY) {
        this.lastRoadPos = [lastPosX, lastPosY];
    },

    setRoadPosition: function (i, j, roadType) {
        if (roadType) {
            if (this.playground2D[i][j] !== null) {
                this.playground2D[i][j] += `|${roadType}`;
            } else {
                this.playground2D[i][j] = roadType;
            }
        }else{
            const splitted = this.playground2D[i][j].split('|');
            if (splitted.length === 2){
                this.playground2D[i][j] = splitted[0];
            }else{
                this.playground2D[i][j] = null;
            }
        }
    },

    calculateDefaultDirection: function (i, j) {
        if (j === 0) {
            return "right";
        } else if (j === this.playground2D[0].length - 1) {
            return "left";
        } else if (i === 0) {
            return "down";
        } else if (i === this.playground2D.length - 1) {
            return "up";
        }
    },

    calculateDefaultDirectionOnMedium: function (){
        const rowMin = new Array(this.c).fill(null).map((item, i) => [0, i]);
        const rowMax = new Array(this.c).fill(null).map((item, i) => [this.r-1, i]);
        const colMin = new Array(this.r).fill(null).map((item, i) => [i, 0]);
        const colMax = new Array(this.r).fill(null).map((item, i) => [i, this.c-1]);

        const foundedInColMin = colMin.find(pos => pos[0] === this.sR && pos[1] === this.sC);
        const foundedInColMax = colMax.find(pos => pos[0] === this.sR && pos[1] === this.sC);
        const foundedInRowMin = rowMin.find(pos => pos[0] === this.sR && pos[1] === this.sC);
        const foundedInRowMax = rowMax.find(pos => pos[0] === this.sR && pos[1] === this.sC);

        if (foundedInColMin){
            return "right";
        }else if (foundedInColMax){
            return "left";
        }else if(foundedInRowMin){
            return "down";
        }else if (foundedInRowMax){
            return "up";
        }
    },

    getDirection: function () {
        return this.direction;
    },

    setDirection: function (dir) {
        this.direction = dir;
    },

    reversalMove: function (deletedRoadCell){
        const letterElem = deletedRoadCell.querySelector('p');
        if (letterElem){
            const letter = letterElem.innerHTML;
            const index = this.actualWord.lastIndexOf(letter);
            const letterSpan = this.actualWordElem.children[index];

            letterSpan.classList.remove('correct', 'incorrect');
            letterSpan.innerHTML = "_ ";
            this.actualWord = this.actualWord.slice(0, -1);
        }

        this.car.setRoads(this.getRoadsToLastLetter());
        this.car.moveCar();
    },

    getRoadsToLastLetter: function (){
        let word = "";
        let count = false;
        const roadsForCar = this.roads.map(road => {
            const cell = document.querySelectorAll(`[data-row="${road[0]}"][data-col="${road[1]}"]`)[0];
            if (road[0] === this.car.r && road[1] === this.car.c){
                count = true;
                return null;
            }
            if (word === this.actualWord || (!count && !(this.car.r === this.sR && this.car.c === this.sC))){
                return null;
            }
            if (cell.querySelector('p')){
                word += cell.querySelector('p').innerHTML;
            }
            return cell;
        });

        return roadsForCar.filter(road => road !==  null);
    },

    getRoadsToFinish: function (){
        let word = "";
        const roadsForCar = this.roads.map(road => {
            const cell = document.querySelectorAll(`[data-row="${road[0]}"][data-col="${road[1]}"]`)[0];
            if (cell.querySelector('p')){
                word += cell.querySelector('p').innerHTML;
                return null;
            }
            if (word === this.actualWord){
                return cell;
            }
            return null;
        });

        return roadsForCar.filter(road => road !==  null);
    },

    setActualWord: function (letter) {
        const index = this.actualWordElem.innerText.indexOf("_");
        const letterSpan = this.actualWordElem.children[index];
        letterSpan.innerHTML = letter;
        this.actualWord += letter;

        if (this.isActualWordCorrect(this.actualWordElem.innerText.replaceAll(/[_ ]/g, ""))) {
            letterSpan.classList.add("correct");
            this.car.setRoads(this.getRoadsToLastLetter());
            this.car.moveCar();
        } else {
            letterSpan.classList.add("incorrect");
        }
    },

    isActualWordCorrect: function (substring) {
        return this.searchedWordElem.innerHTML.startsWith(substring);
    },

    checkGameCompletion: function (r, c) {
        if (this.playground2D[r][c].indexOf("F") === -1) {
            return;
        }

        if (this.isGameCompleted()) {
            this.car.setRoads(this.getRoadsToFinish());
            this.car.moveCar((stopped) => {
                if (stopped) {
                    clearInterval(this.saveInterval);
                    this.setUserGameCompletion(true);
                    this.saveGame(true);
                    this.showSuccessAlert();
                }
            });
        } else {
            this.showFailedAlert();
        }
    },

    nextGame: function () {
        task.next();
        task.runGame();
    },

    isGameCompleted: function () {
        return this.isActualWordCorrect(this.actualWord) && this.actualWord.length === this.searchedWordElem.innerText.length;
    },

    showSuccessAlert: function () {
        Swal.fire({
            title: 'Hurra!',
            text: 'Uspesne si prekonal toto slovo, sup sup na nasledovne',
            icon: 'success',
            showCancelButton: true,
            cancelButtonText: 'Zrusit',
            confirmButtonText: 'Dalsie slovo prosim!'
        }).then(async (result) => {
            if (result.isConfirmed) {
                this.nextGame();
            }
        });
    },

    showFailedAlert: function () {
        Swal.fire({
            title: 'Ooops!',
            text: 'Niekde mas chybu',
            icon: 'error',
            confirmButtonText: 'Upravit zadanie'
        });
    },

    addRoad: function (road){
        this.roads.push(road);
    },

    runSaving: function () {
        if (this.userGame.finished){
            return;
        }

         this.saveInterval = window.setInterval(() => {
             this.saveGame();
         }, 1000);
    },

    setUserGameCompletion: function (completed){
        this.userGame.finished = completed;
    },

    saveGame: function (completed = false){
        const csrf = document.getElementById('csrf').value;
        const serializedObj = task.gameSerialization.stringify(this);
        const gameId = this.game.id;
        const groupId = this.userGame.groupId;
        const taskId = this.userGame.taskId;
        const params = JSON.stringify({gameId, groupId, taskId, state: serializedObj, completed});

        fetch('/game/save', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'csrf-token': csrf,
            },
            body: params
        })
            .then(result => {
                return result.json();
            })
            .then(data => {
                if (data.responseOk){

                }
            })
            .catch(err => {
                console.log("error: ", err);
            });
    },
}