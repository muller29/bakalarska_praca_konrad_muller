function Image(url, tag, name, imgCount, used=null) {
    this.url = url;
    this.tag = tag;
    this.name = name;
    this.imgCount = imgCount;
    this.used = used;
}

extend(DragAndDrop, Image);

Image.prototype = {
    onDragStart: function (event) {
        DragAndDrop.prototype.onDragStart.call(this, event);
    },

    createImg: function () {
        let wrapper = document.createElement("DIV");
        wrapper.className = "roads-div";

        let image = document.createElement("IMG");
        image.classList.add("roads", "draggable");
        image.setAttribute("draggable", "true");
        image.setAttribute("src", this.url);
        image.setAttribute("data-tag", this.tag);
        image.setAttribute("id", this.imgCount);
        image.setAttribute('data-name', this.name);
        if (this.used){
            image.setAttribute('data-used', this.used);
        }

        image.addEventListener("dragstart", this.onDragStart, false);

        wrapper.appendChild(image);
        return wrapper;
    }
}