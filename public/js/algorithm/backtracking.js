function Backtracking (graph, wordLength) {
    this.graph = graph;
    this.riesenie = [];
    this.visitedX = 0;
    this.wordLength = wordLength;
    this.min = 1000;
    this.pocitadlo = 0;
    this.visited = [];
    this.path = [];
}

Backtracking.prototype = {
    constructor: Backtracking,

    start: function (start){
        this.visited.push(start);
        this.path.push(start);
        this.backtrack(start);
        return this.riesenie;
    },

    backtrack: function (actualNode, path, visited){
        if (this.visitedX === this.wordLength && actualNode.letter === 'F'){
            if (this.min > this.pocitadlo){
                this.min = this.pocitadlo;
                this.riesenie = [...this.path];
            }
        }else{
            const neighbors = this.graph.neighbors(actualNode);
            for (let i = 0; i < neighbors.length; i++) {
                const neighbor = neighbors[i];

                if (!this.visited.includes(neighbor)){
                    if (neighbor.letter === "x"){
                        this.visitedX++;
                    }
                    this.pocitadlo++;
                    this.visited.push(neighbor);
                    this.path.push(neighbor);

                    this.backtrack(neighbor);

                    this.visited = this.visited.filter(n => n !== neighbor);
                    this.path = this.path.filter(n => n !== neighbor);
                    this.pocitadlo--;
                    if (neighbor.letter === "x"){
                        this.visitedX--;
                    }
                }
            }
        }
    }
}