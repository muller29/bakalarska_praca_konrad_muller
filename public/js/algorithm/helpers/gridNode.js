function GridNode(x, y, weight, letter) {
    this.x = x;
    this.y = y;
    this.weight = weight;
    this.letter = letter
}

GridNode.prototype.toString = function() {
    return "[" + this.x + " " + this.y + "]";
};

GridNode.prototype.getCost = function() {
    return this.weight;
};

GridNode.prototype.isWall = function() {
    return this.weight === 0;
};

GridNode.prototype.setWall = function (){
    this.weight = 0;
}