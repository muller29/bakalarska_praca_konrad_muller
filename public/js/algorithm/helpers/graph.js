function Graph(array) {
    this.nodes = [];
    this.diagonal = false;
    this.grid = [];
    for (let x = 0; x < array.length; x++) {
        this.grid[x] = [];

        for (let y = 0, row = array[x]; y < row.length; y++) {
            const node = new GridNode(x, y, this.calculateWeight(row[y]), row[y]);
            this.grid[x][y] = node;
            this.nodes.push(node);
        }
    }
    this.init();
}

Graph.prototype.init = function() {
    this.dirtyNodes = [];
    for (let i = 0; i < this.nodes.length; i++) {
        dijkstrov.cleanNode(this.nodes[i]);
    }
};

Graph.prototype.cleanDirty = function() {
    for (let i = 0; i < this.dirtyNodes.length; i++) {
        dijkstrov.cleanNode(this.dirtyNodes[i]);
    }
    this.dirtyNodes = [];
};

Graph.prototype.markDirty = function(node) {
    this.dirtyNodes.push(node);
};

Graph.prototype.neighbors = function(node, all) {
    const ret = [];
    const x = node.x;
    const y = node.y;
    const letter = node.letter;
    const grid = this.grid;
    const directionsAll = {"up": [x - 1, y], "down": [x + 1, y], "left": [x, y - 1], "right": [x, y + 1], "upRight": [x - 1, y + 1], "upLeft": [x - 1, y - 1], "downLeft": [x + 1, y - 1], "downRight": [x + 1, y + 1]};
    const directions4 = {"up": [x - 1, y], "down": [x + 1, y], "left": [x, y - 1], "right": [x, y + 1]};

    const directions = all ? directionsAll : directions4;

    for (const dir in directions) {
        if (letter && letter.indexOf('verticalRoad') !== -1 && ["left", "right"].includes(dir)){
            continue;
        }

        if (letter && letter.indexOf('horizontalRoad') !== -1 && ["up", "down"].includes(dir)){
            continue;
        }

        const [xx, yy] = directions[dir];
        if (!this.outOfRange(xx, yy)){
            ret.push(grid[xx][yy]);
        }
    }

    return ret;
};

Graph.prototype.outOfRange = function (r, c){
    return r < 0 || r >= this.grid.length || c < 0 || c >= this.grid[0].length;
}

Graph.prototype.calculateWeight = function (letter){
    switch (letter){
        case "x":
            return 1;
        case "S":
            return 5;
        case "F":
            return 2;
        default:
            return 3;
    }
}

Graph.prototype.toString = function() {
    const graphString = [];
    const nodes = this.grid;
    for (let x = 0; x < nodes.length; x++) {
        const rowDebug = [];
        const row = nodes[x];
        for (let y = 0; y < row.length; y++) {
            rowDebug.push(row[y].weight);
        }
        graphString.push(rowDebug.join(" "));
    }
    return graphString.join("\n");
};