function BFS(graph){
    this.graph = graph;
}

BFS.prototype = {
    constructor: BFS,
    search: function (graph, start, end){
        if (!this.neighbors[start] || !this.neighbors[start].length) {
            return [start]
        }

        var results = {"nodes": []},
            queue = this.neighbors[start],
            count = 1

        while(queue.length) {
            var node = queue.shift()
            if (!results[node] || !results[node].visited) {
                results[node] = {visited: true, steps: count}
                results["nodes"].push(node)
                if (this.neighbors[node]) {
                    if (this.neighbors[node].length) {
                        count++
                        queue.push(...this.neighbors[node])
                    } else {
                        continue
                    }
                }
            }
        }
        return results
    },

    shortestPath(start, end) {
        var queue = [start],
            visited = {},
            predecessor = {},
            tail = 0,
            letterCount = 0,
            path

        if (start === end && letterCount === 4) {
            return [start, end]
        }

        while(tail < queue.length) {
            let u = queue[tail++]

            const neighbors = this.graph.neighbors(u);
            if (u.letter === "x" && predecessor[u]){
                letterCount++;
            }
            for(var i = 0; i < neighbors.length; ++i) {
                var v = neighbors[i]
                if (visited[v]) {
                    continue
                }
                visited[v] = true
                if (v === end) {
                    path = [ v ]   // If so, backtrack through the path.
                    while (u !== start) {
                        path.push(u)
                        u = predecessor[u]
                    }
                    path.push(u)
                    path.reverse()
                    return path
                }
                predecessor[v] = u
                queue.push(v)
            }
        }

        return path
    }
}