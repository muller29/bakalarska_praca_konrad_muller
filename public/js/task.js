if(window.Prototype) {
    delete Object.prototype.toJSON;
    delete Array.prototype.toJSON;
    delete Hash.prototype.toJSON;
    delete String.prototype.toJSON;
}

const getTask = () => {
    const taskId = document.getElementById('taskId').value;
    const groupId = document.getElementById('groupId').value;
    const csrf = document.getElementById('csrf').value;
    const params = {taskId, groupId};

    return fetch('/ulohy', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        },
        body: JSON.stringify(params)
    })
        .then(result => {
            return result.json();
        })
        .then(data => {
            if (data.responseOk) {
                return data;
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}

String.prototype.replaceAt = function (index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
}

var task = null;

getTask().then((resp) => {
    task = new Task(resp.task, resp.userGames, resp.currentUserId);
    task.checkCompleted();
    task.runGame();
})

function Task(task, userGames, userId) {
    this.task = task;
    this.userGames = userGames;
    this.actualGame = null;
    this.current = 0;
    this.currentUserId = userId;
    this.gameSerialization = new Resurrect({
        prefix: '__#',
    });

    this.setTaskName();
    this.setMaxGameCount();
}

Task.prototype = {
    constructor: Task,

    setTaskName: function (){
        document.getElementById('taskName').innerHTML = this.task.name;
    },

    setMaxGameCount: function (){
      document.getElementById('maxGameCount').innerHTML = this.userGames.length;
    },

    setActualGameNumber: function (){
      document.getElementById('actualGame').innerHTML = this.current + 1;
    },

    next: function () {
        ++this.current;
    },

    checkCompleted: function (){
      const allCompleted = this.userGames.map(uGame => uGame.finished).every(Boolean);
      if (allCompleted){
          return;
      }

      while (true) {
          if (!this.userGames[this.current].finished) {
              break;
          }
          this.next();
      }
    },
    runGame: function () {
        if (this.current >= this.userGames.length){
            window.location.href = "/ulohy";
        }
        const userGame = this.userGames[this.current];
        if (userGame.state){
            this.actualGame = this.gameSerialization.resurrect(userGame.state);
            this.actualGame.initialize();
            this.actualGame.car.render();
            this.actualGame.car.moveCar();
        }else {
            this.actualGame = new Game(userGame.game, userGame);
            this.actualGame.scatterWord();
            this.actualGame.initialize();
            this.actualGame.initializeCar();
        }

        this.setActualGameNumber();
    },

    setCurrent: function (index){
        this.current = index;
    }
}