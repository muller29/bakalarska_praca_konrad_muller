const moveLabel = (elem) => {
    if (elem.value.trim() !== "") {
        elem.classList.add('has-val');
    } else {
        elem.classList.remove('has-val')
    }
}

const inputs = document.getElementsByClassName('input100');
for (let i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener('blur', (e) => moveLabel(e.target));
}
var showPass = 0;
const eyeIcon = document.getElementById('showPass');
eyeIcon.addEventListener('click', (e) => {
        const pwdInput = document.getElementById('password');
        if (showPass === 0) {
            pwdInput.setAttribute('type', 'text');
            e.target.classList.remove('zmdi-eye');
            e.target.classList.add('zmdi-eye-off');
            showPass = 1;
        } else {
            pwdInput.setAttribute('type', 'password');
            e.target.classList.add('zmdi-eye');
            e.target.classList.remove('zmdi-eye-off');
            showPass = 0;
        }
    }
);

document.getElementById('studentLoginForm').addEventListener('submit', (e)=>e.preventDefault());

const login = () => {
    const emailAddress = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    const csrf = document.getElementById('csrf').value;
    const params = {
        password: password,
        emailAddress: emailAddress
    }

    fetch('/login', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        },
        body: JSON.stringify(params)
    })
        .then(result => {
            return result.json();
        })
        .then(data => {
            if (data.responseOk) {
                window.location.href += 'ulohy';
            } else {
                document.getElementById('pwdWrapper').classList.add('login-error');
                document.getElementById('emailWrapper').classList.add('login-error');
                const {message} = data;
                const errorNode = document.getElementById('login_error');
                errorNode.innerHTML = message;
                errorNode.style.display = "block";
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}