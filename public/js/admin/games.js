const gameForm = document.getElementById('gameCreationForm');
const gameFormValidation = new FormValidation(gameForm);
gameFormValidation.validate();

const getGames = () => {
    document.getElementById('spinner').classList.add('d-grid');
    setTimeout(function () {
        const csrf = document.getElementById('csrf').value;

        fetch('/admin/games/all', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'csrf-token': csrf,
            }
        })
            .then(result => {
                return result.json();
            })
            .then(data => {
                if (data.responseOk) {
                    const gamesContainer = document.getElementById('games');
                    gamesContainer.innerHTML = "";

                    if (data.games.length === 0) {
                        document.getElementById('emptyRecords').classList.remove('d-none');
                    } else {

                        const deleteUrl = "/admin/game/delete";
                        const recordUrl = "/admin/game";
                        data.games.forEach(t =>
                            gamesContainer.appendChild(card(t.id, `${t.word} (${t.level})`, deleteUrl, recordUrl))
                        );
                    }
                }
                document.getElementById('spinner').classList.add('d-none');
            })
            .catch(err => {
                console.log("error: ", err);
            });
    }, 1300);
}

const setFormValues = (record) => {
    setModal('gameCreationModal');
    document.getElementById('wordInput').value = record.word;
    document.getElementById('rowInput').value = record.rows;
    document.getElementById('colInput').value = record.columns;
    document.getElementById(`${record.level}Radio`).checked = true;
    document.getElementById('isBarrier').checked = record.barrier;
    document.getElementById('modalTitle').innerHTML = "Upravit slovo";

    const submitBtn = document.getElementById('submit2');
    submitBtn.innerHTML = "Upravit";
    submitBtn.setAttribute('data-action', 'edit');

    let hiddenInput;
    if (document.getElementById('gameId')){
        hiddenInput = document.getElementById('gameId');
    }else{
        hiddenInput = document.createElement('input');
        hiddenInput.type = 'hidden';
        hiddenInput.id = "gameId";
    }
    hiddenInput.value = record.id;
    gameForm.appendChild(hiddenInput);
    gameFormValidation.validate();
    toggleModal();
}

const showRecord = (record) => {
    setModal("gameShowModal");
    const g = new Game(record);
    g.scatterWord();
    g.renderPlayGround();
    g.initializeCar();
    toggleModal();
}

const resetCreationForm = () => {
    gameForm.reset();
}

const reloadRecords = () => {
    getGames();
}

getGames();