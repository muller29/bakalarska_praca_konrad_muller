const taskForm = document.getElementById('taskCreationForm');
const taskFormValidation = new FormValidation(taskForm);
taskFormValidation.validate();

const pickerConfig = {
    enableTime: true,
    dateFormat: "Y-m-d H:i",
    time_24hr: true,
}

const fromPicker = flatpickr('#startedAtInput', pickerConfig);
const toPicker = flatpickr('#finishedAtInput', pickerConfig);

let groupsMultiselect;
let gamesMultiselect;

const getTasks = () => {
    document.getElementById('spinner').classList.add('d-grid');
    setTimeout(function() {
        const csrf = document.getElementById('csrf2').value;

        fetch('/admin/tasks/get', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'csrf-token': csrf,
            }
        })
            .then(result => {
                return result.json();
            })
            .then(data => {
                if (data.responseOk) {
                    const parentElem = document.getElementById('tasks');
                    parentElem.innerHTML = "";
                    if (data.tasks.length === 0){
                        document.getElementById('emptyRecords').classList.remove('d-none');
                    }else{
                        const adminSection = { updateUrl: "/admin/task", deleteUrl: "/admin/task/delete" };
                        data.tasks.forEach((task) => parentElem.appendChild(taskCard(task, null, false, adminSection)));
                    }
                }
                document.getElementById('spinner').classList.add('d-none');
            })
            .catch(err => {
                console.log("error: ", err);
            });
    }, 1300);
}

const setFormValues = (record) => {
    setModal('taskCreationModal');
    const {task, groupIds, gameIds} = record;

    document.getElementById('titleInput').value = task.name;
    fromPicker.setDate(new Date(task.from), false, "Y-m-d H:i");
    toPicker.setDate(new Date(task.to), false, "Y-m-d H:i");
    document.getElementById('descriptionInput').value = task.description;
    getGroupOptions(groupIds);
    getGameOptions(gameIds);
    if (document.getElementById('taskId')){
        document.getElementById('taskId').value = task.id;
    }else{
        const hiddenInput = document.createElement('input');
        hiddenInput.type = 'hidden';
        hiddenInput.value = task.id;
        hiddenInput.id = "taskId";
        taskForm.appendChild(hiddenInput);
    }
    taskFormValidation.validate();
    toggleModal()
}

const resetCreationForm = () => {
    toggleModal();
    getTasks();
}

const reloadRecords = () => {
    getTasks();
}

reloadRecords();