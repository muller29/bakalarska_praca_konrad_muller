export const firstNameValidityChecks = [
    {
        isInvalid: function(input) {
            return input.value.length < 3;
        },
        invalidityMessage: 'Dĺžka mena je minimálne 3 znaky',
        element: document.querySelector('label[for="firstNameInput"] ~.input-requirements li:nth-child(1)')
    }
];

export const lastNameValidityChecks = [
    {
        isInvalid: function(input) {
            return input.value.length < 3;
        },
        invalidityMessage: 'Dĺžka mena je minimálne 3 znaky',
        element: document.querySelector('label[for="lastNameInput"] ~.input-requirements li:nth-child(1)')
    }
];

export const emailValidityChecks = [
    {
        isInvalid: function(input) {
            return input.value.length < 3;
        },
        invalidityMessage: 'Dĺžka e-mailu je minimálne 3 znaky',
        element: document.querySelector('label[for="emailAddressInput"] ~.input-requirements li:nth-child(1)')
    },
    {
        isInvalid: function(input) {
            const regex = /\S+@\S+\.\S+/;
            return !regex.test(String(input.value).toLowerCase());
        },
        invalidityMessage: 'Povelené sú len pismená a čísla',
        element: document.querySelector('label[for="emailAddressInput"] ~.input-requirements li:nth-child(2)')
    }
];