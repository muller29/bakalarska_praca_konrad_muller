function FormValidation(form) {
    this.form = form
}

FormValidation.prototype = {
    validate: function () {
        const submit = document.querySelector('button[type="button"]');
        const inputs = document.querySelectorAll('input:not([type="submit"])');

        for (let i = 0; i < inputs.length; i++) {
            if(inputs[i].CustomValidation !== undefined) {
                inputs[i].CustomValidation.checkInput();
            }
        }

        submit.addEventListener('click', this.validate);
        if (this.form) {
            this.form.addEventListener('submit', this.validate);
        }
        else{
            console.log("Nevieme validovať, formulár je undefined");
        }
    }
}