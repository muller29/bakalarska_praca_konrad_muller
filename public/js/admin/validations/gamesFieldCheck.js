export const wordValidityChecks = [
    {
        isInvalid: function(input) {
            return input.value.length < 3;
        },
        invalidityMessage: 'Dĺžka slova je minimálne 3 znaky',
        element: document.querySelector('label[for="wordInput"] ~.input-requirements li:nth-child(1)')
    }
];

export const rowValidityChecks = [
    {
        isInvalid: function(input) {
            return input.value < 1;
        },
        invalidityMessage: 'Šírka je pozitívne celé číslo',
        element: document.querySelector('label[for="rowInput"] ~.input-requirements li:nth-child(1)')
    },

    {
        isInvalid: function(input) {
            return input.value < 3;
        },
        invalidityMessage: 'Minimálna šírka plochy je 3',
        element: document.querySelector('label[for="rowInput"] ~.input-requirements li:nth-child(2)')
    },

    {
        isInvalid: function(input) {
            const word = document.getElementById('wordInput').value;
            const row = input.value;
            const col = document.getElementById('colInput').value

            if (word && row && col){
                const n = parseInt(row, 10) * parseInt(col, 10);
                return n <= parseInt(word.length, 10) * 2;
            }

            return true;
        },
        invalidityMessage: 'Hra je validná',
        element: document.querySelector('label[for="rowInput"] ~.input-requirements li:nth-child(3)')
    }
];

export const colValidityChecks = [
    {
        isInvalid: function(input) {
            return input.value < 1;
        },
        invalidityMessage: 'Výška je pozitívne celé číslo',
        element: document.querySelector('label[for="colInput"] ~.input-requirements li:nth-child(1)')
    },

    {
        isInvalid: function(input) {
            return input.value < 3;
        },
        invalidityMessage: 'Minimálna výška plochy je 3',
        element: document.querySelector('label[for="colInput"] ~.input-requirements li:nth-child(2)')
    },

    {
        isInvalid: function(input) {
            const word = document.getElementById('wordInput').value;
            const col = input.value;
            const row = document.getElementById('rowInput').value

            if (word && row && col){
                const n = parseInt(row, 10) * parseInt(col, 10);
                return n <= parseInt(word.length, 10) * 2;
            }

            return true;
        },
        invalidityMessage: 'Hra je validná',
        element: document.querySelector('label[for="colInput"] ~.input-requirements li:nth-child(3)')
    }
];