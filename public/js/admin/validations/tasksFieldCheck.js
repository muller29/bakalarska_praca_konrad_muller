export const titleValidityChecks = [
    {
        isInvalid: function(input) {
            return input.value.length < 3;
        },
        invalidityMessage: 'Dĺžka slova je minimálne 3 znaky',
        element: document.querySelector('label[for="titleInput"] ~.input-requirements li:nth-child(1)')
    }
];

export const fromPickerValidityChecks = [
    {
        isInvalid: function(input) {
            return input === undefined || input.value === "";
        },
        invalidityMessage: 'Začiatok je povinný údaj',
        element: document.querySelector('label[for="startedAtInput"] ~.input-requirements li:nth-child(1)')
    },
    {
        isInvalid: function(input) {
            const finishedAt = document.getElementById('finishedAtInput');
            const startedAt = input.value;
            return startedAt && finishedAt && new Date(startedAt) > new Date(finishedAt.value);
        },
        invalidityMessage: 'Začiatok nemôže byť neskoršie ako Koniec',
        element: document.querySelector('label[for="startedAtInput"] ~.input-requirements li:nth-child(2)')
    }
];

export const toPickerValidityChecks = [
    {
        isInvalid: function(input) {
            return input === undefined || input.value === "";
        },
        invalidityMessage: 'Koniec je povinný údaj',
        element: document.querySelector('label[for="finishedAtInput"] ~.input-requirements li:nth-child(1)')
    },
    {
        isInvalid: function(input) {
            const startedAt = document.getElementById('startedAtInput');
            const finishedAt = input.value;
            return startedAt && finishedAt && new Date(startedAt.value) > new Date(finishedAt);
        },
        invalidityMessage: 'Koniec nemôže byť skôr ako Začiatok',
        element: document.querySelector('label[for="finishedAtInput"] ~.input-requirements li:nth-child(2)')
    }
];