export const groupNameValidityChecks = [
    {
        isInvalid: function(input) {
            return input.value.length < 3;
        },
        invalidityMessage: 'Dĺžka mena je minimálne 3 znaky',
        element: document.querySelector('label[for="groupNameInput"] ~.input-requirements li:nth-child(1)')
    },
];