const getStatistics = () => {
    const csrf = document.getElementById('csrf').value;

    fetch('/admin/statistics', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        }
    })
        .then(result => {
            return result.json();
        })
        .then(data => {
            if (data.responseOk) {
                Object.keys(data.statistics).map(k => {
                    if (document.getElementById(k)) {
                        document.getElementById(k).innerHTML = data.statistics[k]
                    }
                });
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}

getStatistics();