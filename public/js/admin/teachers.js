const form = document.getElementById('teacherCreationForm');

const formValidation = new FormValidation(form);
formValidation.validate();

const createOrEditTeacher = (self) => {
    const action = self.getAttribute('data-action');

    const firstName = document.getElementById('firstNameInput').value;
    const lastName = document.getElementById('lastNameInput').value;
    const email = document.getElementById('emailAddressInput').value;
    const password = document.getElementById('passwordInput').value;
    const csrf = document.getElementById('csrf').value;
    const userId = document.getElementById('userId')?.value;

    let params = {
        firstName: firstName,
        lastName: lastName,
        emailAddress: email,
        userType: "teacher"
    }
    const pw = action === "edit" ? {password: password, userId} : action === "create" ? {password: password} : {}
    params = {...params, ...pw}

    const url = action === "create" ? '/admin/teachers/create' : '/admin/teachers/update';

    fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        },
        body: JSON.stringify(params)
    })
        .then(result => {
            return result.json();
        })
        .then(data => {
            if (data.responseOk) {
                Swal.fire({
                    title: 'Success!',
                    text: `${action === "create" ? "Vytvorenie" : "Editácia"} učiteľa prebehlo úspešne`,
                    icon: 'success',
                    confirmButtonText: 'Ok'
                });
                form.reset();
                if (action === "edit"){
                    const submitBtn = document.getElementById('submit');
                    submitBtn.innerHTML = "Vytvoriť";
                    submitBtn.setAttribute('data-action', 'create');
                }
                getTeachers();
            } else {
                if (data.validationErrors) {
                    data.validationErrors.forEach(error => {
                        const elem = document.getElementById(`${error.param}Input`);
                        elem.classList.add('error');
                    });
                }
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}

const getTeachers = () => {
    const csrf = document.getElementById('csrf').value;

    fetch('/admin/teachers/all', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        }
    })
        .then(result => {
            return result.json();
        })
        .then(data => {
            if (data.responseOk) {
                const teachersContainer = document.getElementById('teachers');
                const deleteUrl = "/admin/teacher/delete";
                const recordUrl = "/admin/teacher";
                teachersContainer.innerHTML = "";
                data.teachers.forEach(t =>
                    teachersContainer.appendChild(card(t.id, `${t.firstName} ${t.lastName}`, deleteUrl, recordUrl))
                );
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}

const setFormValues = (record) => {
    const email = document.getElementById('emailAddressInput')
    email.value = record.email;
    const firstName = document.getElementById('firstNameInput')
    firstName.value = record.firstName;
    const lastName = document.getElementById('lastNameInput')
    lastName.value = record.lastName;

    const submitBtn = document.getElementById('submit');
    submitBtn.innerHTML = "Upravit";
    submitBtn.setAttribute('data-action', 'edit');

    let hiddenInput;
    if (document.getElementById('userId')){
        hiddenInput = document.getElementById('userId');
    }else{
        hiddenInput = document.createElement('input');
        hiddenInput.type = 'hidden';
        hiddenInput.id = "userId";
    }
    hiddenInput.value = record.id;

    form.appendChild(hiddenInput);

    // trigger validations
    firstName.CustomValidation.checkInput();
    lastName.CustomValidation.checkInput();
    email.CustomValidation.checkInput();
}

const showRecord = (record) => {
    document.getElementById('userName').innerHTML = `${record.firstName} ${record.lastName}`;
    document.getElementById('userEmail').innerHTML = `${record.email}`;
    toggleModal();
}

const reloadRecords = () => {
    getTeachers();
}

setModal();
getTeachers();