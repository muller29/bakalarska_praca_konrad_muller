const form = document.getElementById('groupCreationForm');

const formValidation = new FormValidation(form);
formValidation.validate();
let instance;
let instance2;

const getStudents = () => {
    const csrf = document.getElementById('csrf').value;

    return fetch(`/admin/students/forLoggedInUser`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        }
    })
        .then(result => {
            return result.json();
        })
        .then(response => {
            if (response.responseOk) {
                return response.students;
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}

const getTeachers = () => {
    const csrf = document.getElementById('csrf').value;
    const role = document.getElementById('role')?.value;

    if (!role || role === "teacher") {
        return new Promise((resolve, reject) => {
            resolve(null)
        });
    }

    return fetch('/admin/teachers/forAdmin', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        }
    })
        .then(result => {
            return result.json();
        })
        .then(data => {
            if (data.responseOk) {
                return data.teachers;
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}

const students = getStudents().then((students) => {
    instance = new SelectPure(".group-multiselect", {
        options: students,
        multiple: true,
        autocomplete: true,
        icon: "fa fa-times",
        placeholder: "Priraďte žiakov do skupiny",
    });
});

const teachers = getTeachers().then((teachers) => {
    if (teachers) {
        instance2 = new SelectPure(".group-owner-select", {
            options: teachers,
            icon: "fa fa-times",
        });
    }
});

const createOrEditGroup = (self) => {
    const action = self.getAttribute('data-action');

    const groupName = document.getElementById('groupNameInput').value;
    const userIds = instance.value();
    const ownerId = instance2 ? instance2.value() : null;

    const csrf = document.getElementById('csrf').value;
    const groupId = document.getElementById('groupId')?.value;

    let params = {groupName, userIds, ownerId};

    const editParams = action === "edit" ? {groupId} : {action: "createGroup"};
    params = {...params, ...editParams}

    const url = action === "create" ? '/admin/groups/create' : '/admin/groups/update';

    fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        },
        body: JSON.stringify(params)
    })
        .then(result => {
            return result.json();
        })
        .then(data => {
            if (data.responseOk) {
                Swal.fire({
                    title: 'Success!',
                    text: `${action === "create" ? "Vytvorenie" : "Editácia"} skupiny prebehlo úspešne`,
                    icon: 'success',
                    confirmButtonText: 'Ok'
                });
                resetCreationForm();
                getGroups();
            } else {
                if (data.validationErrors) {
                    data.validationErrors.forEach(error => {
                        const elem = document.getElementById(`${error.param}Input`);
                        if (error.param === "groupName" && error.value.length >= 3){
                            document.querySelector('label[for="groupNameInput"] ~.input-requirements li:nth-child(2)').classList.add('invalid')
                        }
                        elem.classList.add('error');
                    });
                }
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}

const getGroups = () => {
    const csrf = document.getElementById('csrf').value;

    document.getElementById('spinner').classList.add('d-grid');
    setTimeout(function () {
        fetch('/admin/groups/all', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'csrf-token': csrf,
            }
        })
            .then(result => {
                return result.json();
            })
            .then(data => {
                if (data.responseOk) {
                    const groupsContainer = document.getElementById('groups');
                    const deleteUrl = "/admin/group/delete";
                    const recordUrl = "/admin/group";
                    groupsContainer.innerHTML = "";
                    data.groups.forEach(t =>
                        groupsContainer.appendChild(card(t.id, t.name, deleteUrl, recordUrl, t.users.length))
                    );
                    document.getElementById('spinner').classList.add('d-none');
                }
            })
            .catch(err => {
                console.log("error: ", err);
            });
    }, 1300);
}

const setFormValues = async (record) => {
    const groupName = document.getElementById('groupNameInput');
    groupName.value = record.name;

    document.getElementById('groupMultiselect').innerHTML = "";
    instance = new SelectPure(".group-multiselect", {
        options: await getStudents(),
        multiple: true,
        autocomplete: true,
        icon: "fa fa-times",
        placeholder: "Priraďte žiakov do skupiny",
        value: record.users.map(u => u.id.toString()),
    });

    if (document.getElementById('ownerSelect')) {
        document.getElementById('ownerSelect').innerHTML = "";
        instance2 = new SelectPure(".group-owner-select", {
            options: await getTeachers(),
            icon: "fa fa-times",
            value: record.ownedBy.id.toString(),
        });
    }

    const submitBtn = document.getElementById('submit');
    submitBtn.innerHTML = "Upravit";
    submitBtn.setAttribute('data-action', 'edit');

    const cardTitle = document.getElementById('cardTitle');
    cardTitle.innerHTML = "Uprava skupiny";

    const hiddenInput = document.createElement('input');
    hiddenInput.type = 'hidden';
    hiddenInput.value = record.id;
    hiddenInput.id = "groupId";
    form.appendChild(hiddenInput);

    // trigger validations
    groupName.CustomValidation.checkInput();
}

const showRecord = (record) => {
    document.getElementById('groupName').innerHTML = record.name;
    document.getElementById('ownedBy').innerHTML = `${record.ownedBy.firstName} ${record.ownedBy.lastName} ${record.ownedBy?.role?.name === "admin" ? "(admin)" : ""}`;
    const userContainer = document.getElementById('usersContainer');
    userContainer.innerHTML = "";

    record.users.forEach(user => {
        userContainer.appendChild(userCard(user));
    });

    toggleModal();
}

const reloadRecords = () => {
    getGroups();
}

const resetCreationForm = () => {
    form.reset();
    instance.reset();
    instance2 ? instance2.reset() : null;
    const cardTitle = document.getElementById('cardTitle');
    cardTitle.innerHTML = "Vytváranie skupiny";

    const submitBtn = document.getElementById('submit');
    submitBtn.innerHTML = "Vytvorit";
    submitBtn.setAttribute('data-action', 'create');

    // document.getElementById('groupId').remove();
}

setModal();
getGroups();