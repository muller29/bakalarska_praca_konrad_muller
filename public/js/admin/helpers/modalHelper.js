let modal;

const toggleModal = () => {

    const backdrop = document.getElementById('backdrop');

    modal.classList.toggle('show');
    backdrop.classList.toggle('show');
}

const setModal = (id) => {
    modal = document.getElementById(id ? id : 'modal-default');

    window.onclick = (event) => {
        if (event.target === modal) {
            toggleModal();
        }
    }

    registerModalClose();
}

const registerModalClose = () => {
    const closeIcons = document.getElementsByClassName('modalIconClose');
    if (!_.isEmpty(closeIcons)) {
        Array.from(closeIcons).forEach(elem => {
            elem.addEventListener('click', toggleModal);
        });
    }
    if (document.getElementById('modalButtonClose')){
        document.getElementById('modalButtonClose').addEventListener('click', toggleModal);
    }
}

