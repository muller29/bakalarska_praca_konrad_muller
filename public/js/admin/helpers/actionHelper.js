 const getRecord = (id, url) => {
    const csrf = document.getElementById('csrf').value;

    return fetch(`${url}/${id}`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        }
    })
        .then(result => {
            return result.json();
        })
        .then(response => {
            if (response.responseOk) {
                return response.data;
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}

const deleteAction = (id, url) => {
    const csrf = document.getElementById('csrf').value;
    const params = { id };

    fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        },
        body: JSON.stringify(params)
    })
        .then(result => {
            return result.json();
        })
        .then(data => {
            if (data.responseOk) {
                Swal.fire({
                    title: 'Success!',
                    text: 'Záznam bol úspešne vymazaný.',
                    icon: 'success',
                    confirmButtonText: 'Ok'
                });
                reloadRecords();
            } else {
                Swal.fire({
                    title: 'Error!',
                    text: 'Pri vymazaní záznamu sa nastala chyba. Prosím kontaktujte Vášho dodávateľa.',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                })
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}