const isBetween = (startAt, finishAt) => {
    const startedAt = new Date(startAt);
    const finishedAt = new Date(finishAt);

    const actualDate = new Date();

    return startedAt <= actualDate && finishedAt >= actualDate;
}

const card = (id, name, url, recordUrl, count = null) => {
    const cardBody = document.createElement('div')
    cardBody.classList.add('card-body', 'tag');

    const actions = actionsContainer(id, url, recordUrl);
    const personName = document.createElement('p');
    personName.classList.add('m-0');
    const textNode = document.createTextNode(count ? `${name} (${count})` : name);
    personName.appendChild(textNode);

    cardBody.appendChild(personName);
    cardBody.appendChild(actions);
    return cardBody;
}

const actionsContainer = (id, url, recordUrl) => {
    const actionsContainer = document.createElement('div');
    const showAction = createAction(id, "show", "Otvorit", "fa-eye", null, recordUrl);
    const updateAction = createAction(id, "edit", "Upravit", "fa-edit", url, recordUrl);
    const deleteAction = createAction(id, "delete", "Zmazat", "fa-trash-alt", url);
    actionsContainer.appendChild(showAction);
    actionsContainer.appendChild(updateAction);
    actionsContainer.appendChild(deleteAction);
    return actionsContainer;
}

const createAction = (id, action, title, iconName, url, recordUrl=null) => {
    const button = document.createElement('button');
    button.title = title;
    button.classList.add('btn', 'icon');

    const icon = document.createElement('i')
    icon.classList.add('far', iconName);
    button.appendChild(icon);

    assignEventListenerToActions(id, action, button, url, recordUrl);
    return button;
}

const assignEventListenerToActions = (id, action, button, url, recordUrl) => {
    switch (action) {
        case "show":
            button.addEventListener('click', () => showEventListener(id, recordUrl));
            break;
        case "edit":
            button.addEventListener('click', () => editEventListener(id, recordUrl));
            break;
        case "delete":
            button.addEventListener('click', () => deleteEventListener(id, url));
            break;
        default:
            return
    }
}

const deleteEventListener = (id, url) => {
    Swal.fire({
        title: 'Naozaj chcete vymazať tento záznam?',
        text: "Po vymazaní záznamu už nebudete vedieť vrátiť zmeny!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Zrušiť',
        confirmButtonText: 'Áno, vymazať!'
    }).then(async(result) => {
        if (result.isConfirmed) {
            await deleteAction(id, url);
        }
    })
}

const editEventListener = (id, recordUrl) => {
    getRecord(id, recordUrl)
        .then(record => {
            setFormValues(record);
        });
}

const showEventListener = (id, recordUrl) => {
    getRecord(id, recordUrl)
        .then(record => {
            showRecord(record);
        });
}

const userCard = (user) => {
    const column = document.createElement('div');
    column.classList.add('at-column');

    const atUser = document.createElement('div');
    atUser.classList.add('at-user');


    const img = document.createElement('img');
    img.src = "/dist/img/user2-160x160.jpg";
    const avatar = document.createElement('div');
    avatar.classList.add('at-user__avatar');
    avatar.appendChild(img);
    const name = document.createElement('div');
    name.classList.add('at-user__name');
    name.appendChild(document.createTextNode(`${user.firstName} ${user.lastName}`));
    const email = document.createElement('div');
    email.classList.add('at-user__title');
    email.appendChild(document.createTextNode(user.email));

    atUser.appendChild(avatar);
    atUser.appendChild(name);
    atUser.appendChild(email);

    column.appendChild(atUser);
    return column;
}

const taskCard = (record, group, addBtn = false, adminSection = {}) => {
    const wrapper = document.createElement('div');
    wrapper.classList.add('card-wrapper');

    const card = document.createElement('div');
    card.classList.add('clash-card', 'barbarian');

    const cardImage = document.createElement('div');
    cardImage.classList.add('clash-card__image', 'clash-card__image--barbarian');
    cardImage.style.background = "url(/dist/img/pravopis.jpg) no-repeat center / cover";


    const actualDate = new Date();
    if (addBtn){
        if (actualDate < new Date(record.to)) {
            const url = !isBetween(record.from, record.to) ? "#" : `/skupiny/${group.id}/ulohy/${record.id}`;
            const playBtn = createPlayButton(url);
            cardImage.appendChild(playBtn);
        }else{
            const ribbon = document.createElement('div');
            ribbon.classList.add("ribbon", "ribbon-top-right")

            const span = document.createElement('span');
            span.appendChild(document.createTextNode("Staré!"));
            ribbon.appendChild(span);
            card.appendChild(ribbon);
        }
    }

    if (!_.isEmpty(adminSection)){
        const {updateUrl, deleteUrl} = adminSection
        const editDeleteBtn = createEditAndDeleteBtns(record.id, updateUrl, deleteUrl);
        cardImage.appendChild(editDeleteBtn);
    }else{
        if (actualDate < new Date(record.from) && actualDate < new Date(record.to) ) {
            const div = document.createElement('div');
            div.classList.add('countdown');

            const diff = new Date(record.from) - actualDate
            const countdownTimer = new CountDownTimer(diff/1000, 1000, div);
            countdownTimer.start();
            card.appendChild(div)
        }
    }

    const cardLevel = document.createElement('div');
    cardLevel.classList.add('clash-card__level', 'clash-card__level--barbarian');
    cardLevel.appendChild(document.createTextNode('Uloha'));

    const cardName = document.createElement('div');
    cardName.classList.add('clash-card__unit-name');
    cardName.appendChild(document.createTextNode(record.name));

    const cardDescription = document.createElement('div');
    cardDescription.classList.add('clash-card__unit-description');
    cardDescription.appendChild(document.createTextNode(record.description));

    const unitStat = document.createElement('div');
    unitStat.classList.add('clash-card__unit-stats', 'clash-card__unit-stats--barbarian');

    const stat1 = taskDateContainer(record.from, "Zaciatok");
    const stat3 = taskDateContainer(record.to, "Koniec", true);

    unitStat.appendChild(stat1);
    unitStat.appendChild(stat3);

    card.appendChild(cardImage);
    card.appendChild(cardLevel);
    card.appendChild(cardName);
    card.appendChild(cardDescription);
    card.appendChild(unitStat);

    wrapper.appendChild(card);
    return wrapper;
}

const taskDateContainer = (date, label, isLast=false) => {
    const container = document.createElement('div');
    container.classList.add('one-third');
    if (isLast){
        container.classList.add('no-border');
    }

    const dateString = new Date(date).toLocaleDateString('sk-SK');
    const dateTimeString = new Date(date).toLocaleTimeString('sk-SK', { hour: '2-digit', minute: '2-digit' });

    const statDate = document.createElement('div');
    statDate.classList.add('stat');
    statDate.appendChild(document.createTextNode(dateString));
    statDate.style.marginBottom = "0px";

    const statTime = document.createElement('div');
    statTime.classList.add('stat');
    statTime.appendChild(document.createTextNode(dateTimeString));

    const statValue = document.createElement('div');
    statValue.classList.add('stat-value');
    statValue.appendChild(document.createTextNode(label));

    container.appendChild(statDate);
    container.appendChild(statTime);
    container.appendChild(statValue);

    return container;
}

const createEditAndDeleteBtns = (id, updateUrl, deleteUrl) => {
    const wrp = document.createElement('div');
    wrp.classList.add('actions-wrapper');

    const updateAction = createAction(id, "edit", "Upravit", "fa-edit", deleteUrl, updateUrl);
    const deleteAction = createAction(id, "delete", "Zmazat", "fa-trash-alt", deleteUrl);

    wrp.appendChild(updateAction);
    wrp.appendChild(deleteAction);
    return wrp;
}

const createPlayButton = (url) => {
    const link = document.createElement('a');
    link.href = url;
    link.classList.add("play-btn-wrapper");

    const svg = document.createElementNS("http://www.w3.org/2000/svg", 'svg');
    svg.setAttributeNS(null, "xlink", "http://www.w3.org/1999/xlink");
    svg.setAttributeNS(null, "x", "0px");
    svg.setAttributeNS(null, "y", "0px");
    svg.setAttributeNS(null, "height", "100px");
    svg.setAttributeNS(null, "width", "100px");
    svg.setAttributeNS(null, "viewBox", "0 0 100 100");
    svg.setAttributeNS(null, "enable-background", "new 0 0 100 100");
    svg.setAttributeNS(null, "space", "preserve");
    svg.setAttributeNS(null, "id", "play");

    const path = document.createElementNS("http://www.w3.org/2000/svg", 'path');
    path.setAttributeNS(null, "class", "stroke-solid");
    path.setAttributeNS(null, "fill", "none");
    path.setAttributeNS(null, "stroke", "#ddbe72");
    path.setAttributeNS(null, "d", "M49.9,2.5C23.6,2.8,2.1,24.4,2.5,50.4C2.9,76.5,24.7,98,50.3,97.5c26.4-0.6,47.4-21.8,47.2-47.7C97.3,23.7,75.7,2.3,49.9,2.5");

    const path2 = document.createElementNS("http://www.w3.org/2000/svg", 'path');
    path2.setAttributeNS(null, "class", "icon");
    path2.setAttributeNS(null, "fill", "#ddbe72");
    path2.setAttributeNS(null, "d", "M38,69c-1,0.5-1.8,0-1.8-1.1V32.1c0-1.1,0.8-1.6,1.8-1.1l34,18c1,0.5,1,1.4,0,1.9L38,69z");

    svg.appendChild(path);
    svg.appendChild(path2);

    link.appendChild(svg);
    return link;
}