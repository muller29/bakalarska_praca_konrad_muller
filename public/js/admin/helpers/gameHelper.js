const createOrUpdateGame = (self) => {
    const action = self.getAttribute('data-action');
    const word = document.getElementById('wordInput').value;
    const row = document.getElementById('rowInput').value;
    const col = document.getElementById('colInput').value;
    const barrier = document.getElementById('isBarrier').checked;
    const level = document.querySelector('input[type="radio"][name="level"]:checked').value;

    const csrf = document.getElementById('csrf2').value;
    const gameId = document.getElementById('gameId')?.value;

    let params = {word, row, col, level, barrier};

    const editParams = action === "edit" ? {gameId} : {action: "createGame"};
    params = {...params, ...editParams}

    const url = action === "create" ? '/admin/games/create' : '/admin/games/update';

    fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        },
        body: JSON.stringify(params)
    })
        .then(result => {
            return result.json();
        })
        .then(data => {
            if (data.responseOk) {
                Swal.fire({
                    title: 'Success!',
                    text: `${action === "create" ? "Vytvorenie" : "Editácia"} slova prebehlo úspešne.`,
                    icon: 'success',
                    confirmButtonText: 'Ok'
                });
                resetCreationForm();
                toggleModal();
                getGames();
            } else {
                if (data.validationErrors) {
                    data.validationErrors.forEach(error => {
                        const elem = document.getElementById(`${error.param}Input`);
                        elem.classList.add('error');
                    });
                }
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}