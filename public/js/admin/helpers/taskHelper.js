const createOrUpdateTask = (self) => {
    const action = self.getAttribute('data-action');
    const title = document.getElementById('titleInput').value;
    const startedAt = document.getElementById('startedAtInput').value;
    const finishedAt = document.getElementById('finishedAtInput').value;
    const description = document.getElementById('descriptionInput').value;

    const gameIds = gamesMultiselect.value();
    const groupIds = groupsMultiselect.value();

    const csrf = document.getElementById('csrf').value;
    const taskId = document.getElementById('taskId')?.value;

    let params = {title, description, startedAt, finishedAt, gameIds, groupIds};

    const editParams = action === "edit" ? {taskId} : { action: "createTask"};
    params = {...params, ...editParams};

    const url = action === "create" ? '/admin/tasks/create' : '/admin/tasks/update';

    fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        },
        body: JSON.stringify(params)
    })
        .then(result => {
            return result.json();
        })
        .then(data => {
            if (data.responseOk) {
                Swal.fire({
                    title: 'Success!',
                    text: `${action === "create" ? "Vytvorenie" : "Editácia"} úlohy prebehlo úspešne.`,
                    icon: 'success',
                    confirmButtonText: 'Ok'
                });
                resetCreationForm();
            } else {
                if (data.validationErrors) {
                    data.validationErrors.forEach(error => {
                        const elem = document.getElementById(`${error.param}Input`);
                        elem.classList.add('error');
                    });
                }
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}

const getGroups = () => {
    const csrf = document.getElementById('csrf').value;

    return fetch('/admin/groups/forLoggedInUser', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        }
    })
        .then(result => {
            return result.json();
        })
        .then(data => {
            if (data.responseOk) {
                return data.groups;
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}

const getGames = () => {
    const csrf = document.getElementById('csrf').value;

    return fetch('/admin/games/forLoggedInUser', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        }
    })
        .then(result => {
            return result.json();
        })
        .then(data => {
            if (data.responseOk) {
                return data.games;
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}

const getGroupOptions = (groupIds=[]) => {
    document.getElementById('groupsInput').innerHTML = "";
    getGroups().then((groups) => {
        groupsMultiselect = new SelectPure(".group-multiselect", {
            options: groups,
            value: groupIds,
            multiple: true,
            autocomplete: true,
            icon: "fa fa-times",
            placeholder: "Priradit ulohu ku skupinam",
        });
    });
}

const getGameOptions = (gameIds=[]) => {
    document.getElementById('gamesInput').innerHTML = "";
    getGames().then((games) => {
        gamesMultiselect = new SelectPure(".games-multiselect", {
            options: games,
            value: gameIds,
            multiple: true,
            autocomplete: true,
            icon: "fa fa-times",
            placeholder: "Priradit slovo k ulohe",
        });
    });
}