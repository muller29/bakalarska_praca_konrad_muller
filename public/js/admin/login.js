document.getElementById('adminLoginForm').addEventListener('submit', (e)=>e.preventDefault());

const adminLogin = (_) => {
    const emailAddress = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    const csrf = document.getElementById('csrf').value;
    const params = {
        password: password,
        emailAddress: emailAddress
    }

    fetch('/login', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrf-token': csrf,
        },
        body: JSON.stringify(params)
    })
        .then(result => {
            return result.json();
        })
        .then(data => {
            if(data.responseOk){
                window.location.href += '/dashboard';
            }
            else{
              const { message } = data;
              const errorNode = document.getElementById('login_error');
              errorNode.innerHTML = message;
              errorNode.style.display = "block";
            }
        })
        .catch(err => {
            console.log("error: ", err);
        });
}