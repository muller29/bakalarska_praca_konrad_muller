const pickerConfig = {
    enableTime: true,
    time_24hr: true,
    dateFormat: "Y-m-d H:i",
}

const fromPicker = flatpickr('#startedAtInput', pickerConfig);
const toPicker = flatpickr('#finishedAtInput', pickerConfig);

const gameForm = document.getElementById('gameCreationForm');
const gameFormValidation = new FormValidation(gameForm);
gameFormValidation.validate();

const taskForm = document.getElementById('taskCreationForm');
const taskFormValidation = new FormValidation(taskForm);
taskFormValidation.validate();

let groupsMultiselect;
let gamesMultiselect;

const resetCreationForm = () => {
    gameForm.reset();
    taskForm.reset();
    groupsMultiselect.reset();
    gamesMultiselect.reset();
    getGameOptions();
}

getGroupOptions();
getGameOptions();

setModal('gameCreationModal');