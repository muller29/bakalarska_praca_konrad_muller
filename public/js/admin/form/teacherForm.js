import {CustomValidation} from "../../form/validation.js";
import {
    firstNameValidityChecks,
    lastNameValidityChecks,
    emailValidityChecks
} from "../validations/teachersFieldCheck.js";

const firstNameInput = document.getElementById('firstNameInput');
const lastNameInput = document.getElementById('lastNameInput');
const emailInput = document.getElementById('emailAddressInput')

firstNameInput.CustomValidation = new CustomValidation(firstNameInput);
firstNameInput.CustomValidation.validityChecks = firstNameValidityChecks;

lastNameInput.CustomValidation = new CustomValidation(lastNameInput);
lastNameInput.CustomValidation.validityChecks = lastNameValidityChecks;

emailInput.CustomValidation = new CustomValidation(emailInput);
emailInput.CustomValidation.validityChecks = emailValidityChecks;