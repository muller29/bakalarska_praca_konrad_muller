import {CustomValidation} from "../../form/validation.js";
import { titleValidityChecks, fromPickerValidityChecks, toPickerValidityChecks } from "../validations/tasksFieldCheck.js";

const titleInput = document.getElementById('titleInput');
const startedAtInput = document.getElementById('startedAtInput');
const finishedAtInput = document.getElementById('finishedAtInput');

titleInput.CustomValidation = new CustomValidation(titleInput);
titleInput.CustomValidation.validityChecks = titleValidityChecks;

startedAtInput.CustomValidation = new CustomValidation(startedAtInput);
startedAtInput.CustomValidation.validityChecks = fromPickerValidityChecks;

finishedAtInput.CustomValidation = new CustomValidation(finishedAtInput);
finishedAtInput.CustomValidation.validityChecks = toPickerValidityChecks;

