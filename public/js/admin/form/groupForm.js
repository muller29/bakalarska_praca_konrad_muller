import {CustomValidation} from "../../form/validation.js";
import {groupNameValidityChecks} from "../validations/groupsFieldCheck.js";

const groupNameInput = document.getElementById('groupNameInput');

groupNameInput.CustomValidation = new CustomValidation(groupNameInput);
groupNameInput.CustomValidation.validityChecks = groupNameValidityChecks;
