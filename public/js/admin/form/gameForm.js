import {CustomValidation} from "../../form/validation.js";
import {wordValidityChecks, rowValidityChecks, colValidityChecks} from "../validations/gamesFieldCheck.js";

const wordInput = document.getElementById('wordInput');
const rowInput = document.getElementById('rowInput');
const colInput = document.getElementById('colInput');

wordInput.CustomValidation = new CustomValidation(wordInput);
wordInput.CustomValidation.validityChecks = wordValidityChecks;

rowInput.CustomValidation = new CustomValidation(rowInput);
rowInput.CustomValidation.validityChecks = rowValidityChecks;

colInput.CustomValidation = new CustomValidation(colInput);
colInput.CustomValidation.validityChecks = colValidityChecks;