# Cesta do pravopisu
Bakalárska práca je dostupná online na adrese: https://bakalarska-praca.herokuapp.com/
Prihlasovacie údaje do administrácie nájdete v bode 19. nižšie

## Inštalačný návod (Windows)
1. Stiahnite a nainštalujte si wampserver zo stránky: https://sourceforge.net/projects/wampserver/
2. Spustite wampserver
3. Otvorte phpmyadmin http://localhost/phpmyadmin/
4. Prihláste sa (Username: root, password: )
5. Vytvorte MySQL databázu
6. Stiahnite a nainštalujte si poslednú LTS verziu NodeJS zo stranky https://nodejs.org/en/download/
7. Po inštalácii musíte pridať cestu nodeJS priečinku k environment variables
8. Otvorte cmd a dostante sa pod folder kam chcete ukladať projekt
9. Naklónujte projekt z gitlabu
```shell
git clone https://gitlab.com/muller29/bakalarska_praca_konrad_muller.git
```
10. Otvorte projekt v nejakom IDE (Webstorm, VS code atď.)
11. Spustite príkaz
```shell
npm install
```
12. Po inštalácií knižníc potrebujete nainštalovať ešte Redis. Stiahnite si Redis zo stránky https://riptutorial.com/redis/example/29962/installing-and-running-redis-server-on-windows
13. Rozbalte stiahnutý .zip súbor na disku C
14. Pridajte cestu k priečinku medzi environment variable
15. Otvorte si nový cmd a pustite príkaz
```shell
redis-server
```
16. Pred spustením aplikácie musíte nastaviť ENV premenné. Otvorte si IDE, kde je otvorený projekt a otvorte si súbor config/config.json.
Nastavte development prostredie podľa vašej lokálnej databázy, ako napr.
```json
  "development": {
    "username": "root",
    "password": "",
    "database": "cesta_do_pravopisu",
    "host": "localhost",
    "dialect": "mysql",
    "port": 3306,
    "timezone": "+01:00",
    "dialectOptions": {
      "useUTC": false
    }
  }
```
17. V app.js odkomentujte riadok
```shell
// .sync({ force: true })
```
a zakomentujte
```shell
.sync()
```
tento príkaz pri spustení na novo vytvorí celú databázu
18. Spustite app.js bud pomocou IDE alebo s príkazom
```shell
npm run start
```
19. Po spustení tohto príkazu terminál Vás informuje čo všetko pospúštal a vytvoril. Môžte skúsiť otvoriť stránku na localhost:3000
Ak Vám stránka nabehla, tak otvorte app.js súbor a bod 17. zopakujte, ale opačne. Zakomentujte
```shell
.sync({ force: true })
```
a odkomentujte
```shell
// .sync()
```
20. V terminali kde ste v adresári projektu spustite príkaz
```shell
sequelize-cli db:seed:all
```
Tento príkaz Vám pripraví dummy dáta dopredu. Vytvorí administrátorský účet:
```shell
Email: admin@admin.sk
Heslo: admin
```
21. Administrátorské rozhranie nájdete na localhost:3000/admin
