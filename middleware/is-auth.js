const User = require('../models/user');
const Role = require('../models/role');

module.exports = (req, res, next) => {
    if (req.session && req.session.user) {
        User.findOne({ where: { email: req.session.user.email}, include: [{ model: Role }]})
            .then(user => {
                if (user) {
                    req.user = user;
                    delete req.user.password; // delete the password from the session
                    req.session.user = user;  //refresh the session value
                    res.locals.user = user;

                    next();
                }
            });
    } else {
        return res.redirect('/login');
    }
}