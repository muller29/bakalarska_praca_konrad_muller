const Role = require('../models/role');

module.exports = async (req, res, next) => {
    try {
        const role = await Role.findByPk(req.session.user.roleId);
        if (role.name !== 'student') {
            return res.redirect('/');
        }
        next();
    }
    catch(err){
        console.log("isStudent middleware error");
    }
}