const Role = require('../models/role');
const User = require('../models/user');
const Group = require('../models/group');
const Game = require('../models/game');
const Task = require('../models/task');

const permissionDenied = (next) => {
    const error = new Error("Permission denied");
    error.httpStatusCode = 403;
    return next(error);
}

module.exports = async (req, res, next) => {
    try {
        const role = await Role.findByPk(req.session.user.roleId);

        switch (role.name) {
            case "admin":
                next();
                break;
            case "teacher":
                const userId = ( /student|students|teacher|teachers/g ).test(req.url) && (req.body.id || req.params.userId || req.body.userId);
                const groupId = ( /group|groups/g ).test(req.url) && (req.body.id || req.params.groupId || req.body.groupId);
                const gameId = ( /game|games/g ).test(req.url) && (req.body.id || req.params.gameId || req.body.gameId);
                const taskId = ( /task|tasks/g ).test(req.url) && (req.body.id || req.params.taskId || req.body.taskId);
                if (userId) {
                    const user = await User.findOne({where: {id: userId, createdById: req.session.user.id}});
                    if (user){
                        next()
                        break;
                    }
                } else if ((req.body.userType && req.body.userType === "student")) {
                    next()
                    break;
                } else if (req.body.action && ["createGroup", "createGame", "createTask"].includes(req.body.action)){
                    next()
                    break;
                } else if (groupId){
                    const group = await Group.findOne({where: { id: groupId, createdById: req.session.user.id }});
                    if (group){
                        next()
                        break;
                    }
                } else if (gameId){
                    const game = await Game.findOne({ where: { id: gameId, createdById: req.session.user.id } });
                    if (game){
                        next()
                        break;
                    }
                } else if (taskId){
                    const task = await Task.findOne({ where: { id: taskId, createdById: req.session.user.id } });
                    if (task){
                        next()
                        break;
                    }
                }

                return permissionDenied(next);
            default:
                return permissionDenied(next);
        }
    } catch (err) {
        console.log("isAdmin middleware error");
    }
}