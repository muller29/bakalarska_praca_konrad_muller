const User = require('../models/user');
const Role = require('../models/role');

module.exports = (req, res, next) => {
    if (req.session && req.session.user) {
        User.findOne({ where: { email: req.session.user.email}, include: [{ model: Role }]})
            .then(user => {
                if (user && ['admin', 'teacher'].includes(user.role.name)) {
                    return res.redirect('/admin/dashboard');
                }
                else if(user && user.role.name === 'student'){
                    return res.redirect('/ulohy');
                }
                else{
                    next();
                }
            });
    } else {
        next()
    }
}