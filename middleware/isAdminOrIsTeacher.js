const Role = require('../models/role');

module.exports = async (req, res, next) => {
    try {
        const role = await Role.findByPk(req.session.user.roleId);
        if (role.name !== 'admin' && role.name !== 'teacher') {
            return res.redirect('/login');
        }
        next();
    }
    catch(err){
        console.log("isAdmin middleware error");
    }
}