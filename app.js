const path = require('path');
const fs = require('fs');

const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const csrf = require('csurf');
const flash = require('connect-flash');
const redis = require('redis');
const RedisStore = require('connect-redis')(session);
const helmet = require('helmet');
const compression = require('compression');
const morgan = require('morgan');

const errorController = require('./controllers/error');
const sequelize = require('./util/database');
const User = require('./models/user');
const Role = require('./models/role');
const Group = require('./models/group');
const Task = require('./models/task');
const GroupTask = require('./models/group-task');
const Game = require('./models/game');
const UserGame = require('./models/user-game');
const GroupUser = require('./models/group-user');
const GameTask = require('./models/game-task');

const app = express();
const csrfProtection = csrf();

app.set('view engine', 'ejs');
app.set('views', 'views');

const adminRoutes = require('./routes/admin');
const authRoutes = require('./routes/auth');
const studentRoutes = require('./routes/student');

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' });

// app.use(helmet()); //comment in developmentDm5[[(6dWIDm5[[(6dWI
app.use(compression());
app.use(morgan('combined', { stream: accessLogStream }));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

//Configure redis client
const redisClient = redis.createClient(process.env.REDIS_URL)
redisClient.on('error', function (err) {
    console.log('Could not establish a connection with redis. ' + err);
});
redisClient.on('connect', function (err) {
    console.log('Connected to redis successfully');
});

app.use(
    session({
        key: '69Atu22GZTSyDGW4sf4mMJdJ42436gAs',
        secret: '3dCE84rey8R8pHKrVRedgyEjhrqGT5Hz',
        resave: false,
        saveUninitialized: false,
        store: new RedisStore({ client: redisClient }),
        cookie: {
            secure: false, // if true only transmit cookie over https
            httpOnly: false, // if true prevent client side JS from reading the cookie
            maxAge: 1000 * 60 * 100 // session max age in miliseconds (actually 100min)
        }
    })
);
app.use(csrfProtection);
app.use(flash());

app.use((req, res, next) => {
    if (!req.session.user) {
        return next();
    }
    User.findByPk(req.session.user.id)
        .then(user => {
            req.user = user;
            next();
        })
        .catch(err => console.log(err));
});

app.use((req, res, next) => {
    res.locals.isAuthenticated = req.session.isLoggedIn;
    res.locals.csrfToken = req.csrfToken();
    next();
});

app.use('/admin', adminRoutes);
app.use(authRoutes);
app.use(studentRoutes);

app.get('/500', errorController.get500);

app.use(errorController.get404);

app.use((error, req, res, next) => {
    res.status(error.httpStatusCode).render(error.httpStatusCode.toString(), {
        pageTitle: 'Error',
        path: `/${error.httpStatusCode.toString()}`
    });
});

User.belongsTo(Role, { constraints: true });
User.belongsToMany(Group, { through: GroupUser });
User.belongsToMany(Game, { through: { model: UserGame, unique: false } });
User.hasMany(Group, { as: 'ownedBy', foreignKey: 'createdById' });
User.hasMany(Game, { as: 'gameCreator', foreignKey: 'createdById' });
User.hasMany(Task, { as: 'taskCreator', foreignKey: 'createdById' });
Role.hasMany(User);
Group.belongsToMany(User, { through: GroupUser });
Group.belongsToMany(Task, { through: GroupTask });
Group.belongsTo(User, { as: 'ownedBy', foreignKey: 'createdById' });
Group.hasMany(UserGame, { foreignKey: 'groupId' });
Task.belongsToMany(Group, { through: GroupTask });
Task.belongsToMany(Game, { through: GameTask });
Task.belongsTo(User, { as: 'taskCreator', foreignKey: 'createdById' });
Task.hasMany(UserGame, { foreignKey: 'taskId' });
Game.belongsTo(User, { as: 'gameCreator', foreignKey: 'createdById' });
Game.belongsToMany(Task, { through: GameTask });
Game.belongsToMany(User, { through: { model: UserGame, unique: false } });
UserGame.belongsTo(Task, { foreignKey: 'taskId' });
UserGame.belongsTo(Group, { foreignKey: 'groupId' });
UserGame.belongsTo(Game, { foreignKey: 'gameId' });

sequelize
  //.sync({ force: true })
  .sync()
  .then(() => {
    app.listen(process.env.PORT || 3000);
  })
  .catch(err => {
    console.log(err);
  });
