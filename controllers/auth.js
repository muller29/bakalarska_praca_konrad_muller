const bcrypt = require('bcryptjs');

const User = require('../models/user');
const Role = require('../models/role');

exports.getAdminLogin = (req, res, next) => {
    res.render('admin/auth/login', {
        path: '/admin',
        pageTitle: 'Prihlásenie'
    });
};

exports.postLogin = (req, res, next) => {
    const emailAddress = req.body.emailAddress;
    const password = req.body.password;

    User.findOne({
        where: { email: emailAddress },
        include: [{ model: Role }] }
        )
        .then(user => {
            if (!user) {
                req.flash('error', 'Invalid email or password.');
                res.status(422).json({ message: "E-mailová adresa alebo heslo sa nezhoduje.", responseOk: false });
            }
            bcrypt
                .compare(password, user.password)
                .then(doMatch => {
                    if (doMatch) {
                        req.session.isLoggedIn = true;
                        req.session.user = user;
                        return req.session.save(err => {
                            console.log(err);
                        });
                    }
                    res.status(422).json({ message: "E-mailová adresa alebo heslo sa nezhoduje.", responseOk: false });
                })
                .then(session => {
                    res.status(200).json({ responseOk: true });
                })
                .catch(err => {
                    res.redirect('/login');
                });
        })
        .catch(err => res.status(500).json({ message: 'Pri prihlásení sa nastala chyba.', responseOk: false }));
}

exports.postLogout = (req, res, next) => {
    if (req.session) {
        req.session.destroy(err => {
            console.log(err);
            res.redirect('/');
        });
    }else{
        res.redirect('/');
    }
};