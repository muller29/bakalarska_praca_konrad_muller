const Sequelize = require('sequelize');
const Op = Sequelize.Op

const User = require('../models/user');
const Group = require('../models/group');
const Task = require('../models/task');
const Game = require('../models/game');
const UserGame = require('../models/user-game');

exports.getIndex = (req, res, next) => {
    res.render('student/index', {
        pageTitle: 'Index',
        path: '/'
    });
};

exports.getDashboard = (req, res, next) => {
    res.render('student/dashboard', {
        pageTitle: 'Úlohy',
        path: '/ulohy',
        username: `${req.session.user.firstName} ${req.session.user.lastName}`,
    });
};

exports.getTaskPage = (req, res, next) => {
    res.render('student/task', {
        pageTitle: 'Úlohy',
        path: `/skupiny/${req.params.groupId}/ulohy/${req.params.taskId}`,
        taskId: req.params.taskId,
        groupId: req.params.groupId,
        username: `${req.session.user.firstName} ${req.session.user.lastName}`,
    });
}

exports.postTask = async(req, res, next) => {
    const taskId = req.body.taskId;
    const groupId = req.body.groupId;
    const task = await Task.findOne({ where: { id: taskId }, include: [
            {
                model: Game,
                include: [User]
            }
        ]});
    const user = await User.findOne({where: {id: req.session.user.id}, include: [Game]});
    const userGames = await UserGame.findAll({ where: { userId: user.id, groupId: groupId, taskId: taskId }, include: [Game] });
    return res.status(200).json({ responseOk: true, task: task, userGames: userGames, currentUserId: req.session.user.id });
}

exports.postSaveGame = async(req, res, next) => {
    const currentUser = req.session.user;
    const state = req.body.state;
    const gameId = req.body.gameId;
    const groupId = req.body.groupId;
    const taskId = req.body.taskId;
    const completed = req.body.completed;

    const user = await User.findByPk(currentUser.id);

    let userGame = await UserGame.findOne({ where: { userId: user.id, gameId, groupId, taskId } });
    userGame.state = state;
    userGame.finished = completed;
    await userGame.save();

    return res.status(200).json({ responseOk: true });
}

function compare( a, b ) {
    if ( a.name < b.name ){
        return -1;
    }
    if ( a.name > b.name ){
        return 1;
    }
    return 0;
}


exports.getGroupsWithTasks = async (req, res, next) => {
    const currentUser = req.session.user;
    const user = await User.findByPk(currentUser.id);
    const groupIds = await user.getGroups().map(group => group.id);
    let groupsWithTasks = await Group.findAll({where: {id: {[Op.in]: groupIds}}, include: [Task]});
    groupsWithTasks = groupsWithTasks.filter(group => group.tasks.length !== 0).sort(compare);

    res.status(200).json({responseOk: true, groupsWithTasks});
}