const bcrypt = require('bcryptjs');

const {validationResult} = require('express-validator/check');

const User = require('../models/user');
const Role = require('../models/role');
const Group = require('../models/group');
const Game = require('../models/game');
const Task = require('../models/task');
const UserGame = require('../models/user-game');
const GroupUser = require('../models/group-user');

const Sequelize = require('sequelize');
const Op = Sequelize.Op


const getGameLevel = (level) => {
    switch (level) {
        case 'easy':
            return 1;
        case 'medium':
            return 2;
        case 'hard':
            return 3;
        default:
            return 1;
    }
}

exports.postCreateUser = async (req, res, next) => {
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const emailAddress = req.body.emailAddress;
    const password = req.body.password;
    const userType = req.body.userType;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({responseOk: false, validationErrors: errors.array()})
    }

    const role = await Role.findOne({where: {name: userType}})
    bcrypt
        .hash(password, 12)
        .then(hashedPassword => {
            return User.create({
                firstName: firstName,
                lastName: lastName,
                email: emailAddress,
                password: hashedPassword,
                roleId: role.id,
                createdById: req.session.user.id
            })
        })
        .then(result => {
            res.status(200).json({responseOk: true});
        })
        .catch(err => {
            const error = new Error(err);
            console.log(error);
            error.httpStatusCode = 500;
            return next(error);
        });
}

exports.postEditUser = async (req, res, next) => {
    const userId = req.body.userId;
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const emailAddress = req.body.emailAddress;
    const password = req.body.password;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({responseOk: false, validationErrors: errors.array()})
    }
    const user = await User.findByPk(userId);
    const hashedPwd = await bcrypt.hash(password, 12);
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = emailAddress;
    user.password = hashedPwd;
    await user.save();

    return res.status(200).json({responseOk: true});
}

exports.postDeleteUser = (req, res, next) => {
    const userId = req.body.id;
    User.findByPk(userId)
        .then(user => {
            return user.destroy();
        })
        .then(result => {
            res.status(200).json({responseOk: true})
        })
        .catch(err => console.log(err));
};

exports.getUser = async (req, res, next) => {
    const currentUser = req.session.user;
    const currentUserRole = await Role.findOne({where: {id: currentUser.roleId}});
    const conditions = currentUserRole.name === "teacher" ? {createdById: currentUser.id} : {};
    const userId = req.params.userId;
    const user = await User.findOne({where: {id: userId}, ...{conditions}, include: [{model: Role}]});
    res.status(200).json({responseOk: true, data: user});
}

exports.getTeachers = async (req, res, next) => {
    const role = await Role.findOne({where: {name: 'teacher'}});
    const teachers = await User.findAll({where: {roleId: role.id}});
    res.status(200).json({responseOk: true, teachers: teachers});
}

exports.getStudents = async (req, res, next) => {
    const currentUser = req.session.user;
    const currentUserRole = await Role.findOne({where: {id: currentUser.roleId}});
    const conditions = currentUserRole.name === "teacher" ? {createdById: currentUser.id} : {};
    const role = await Role.findOne({where: {name: 'student'}});
    const students = await User.findAll({where: {roleId: role.id, ...conditions}});
    res.status(200).json({responseOk: true, students: students});
}

exports.getGroup = async (req, res, next) => {
    const currentUser = req.session.user;
    const currentUserRole = await Role.findOne({where: {id: currentUser.roleId}});
    const conditions = currentUserRole.name === "teacher" ? {createdById: currentUser.id} : {};
    const groupId = req.params.groupId;
    const group = await Group.findOne({
        where: {id: groupId, ...conditions},
        include: [
            {
                model: User,
                as: 'ownedBy',
                include: Role
            },
            {
                model: User,
                attributes: ['id', 'firstName', 'lastName', 'email']
            }
        ],
        attributes: [
            "id", "name"
        ]
    });
    res.status(200).json({responseOk: true, data: group});
}

exports.getGame = async (req, res, next) => {
    const currentUser = req.session.user;
    const currentUserRole = await Role.findOne({where: {id: currentUser.roleId}});
    const conditions = currentUserRole.name === "teacher" ? {createdById: currentUser.id} : {};
    const gameId = req.params.gameId;
    const game = await Game.findOne({
        where: {id: gameId, ...conditions},
        include: [
            {
                model: User,
                as: 'gameCreator',
                include: Role
            },
            {
                model: User,
                attributes: ['id', 'firstName', 'lastName', 'email']
            },
            {
                model: Task,
                attributes: ['id', 'name']
            }
        ],
        attributes: [
            "id", "word", "rows", "columns", "level", "barrier"
        ]
    });
    res.status(200).json({responseOk: true, data: game});
}

exports.getTask = async (req, res, next) => {
    const currentUser = req.session.user;
    const currentUserRole = await Role.findOne({where: {id: currentUser.roleId}});
    const conditions = currentUserRole.name === "teacher" ? {createdById: currentUser.id} : {};
    const taskId = req.params.taskId;
    const task = await Task.findOne({
        where: {id: taskId, ...conditions},
        include: [
            {
                model: User,
                as: 'taskCreator',
                include: Role
            },
        ],
        attributes: [
            "id", "name", "from", "to", "description", "image"
        ]
    });

    const groupIds = await task.getGroups().map(group => group.id.toString());
    const gameIds = await task.getGames().map(game => game.id.toString());
    res.status(200).json({responseOk: true, data: {task, groupIds, gameIds}});
}


exports.getGroups = async (req, res, next) => {
    const currentUser = req.session.user;
    const currentUserRole = await Role.findOne({where: {id: currentUser.roleId}});
    const conditions = currentUserRole.name === "teacher" ? {createdById: currentUser.id} : {};
    const groups = await Group.findAll({
        where: {...conditions},
        include: [
            {
                model: User,
                attributes: ['id'],
            },
            {
                model: User,
                as: 'ownedBy'
            },
        ],
        attributes: [
            "id", "name"
        ],
        group: [
            'id', 'users.id'
        ]
    });
    res.status(200).json({responseOk: true, groups: groups});
}

exports.getStatistics = async (req, res, next) => {
    const currentUser = req.session.user;
    const currentUserRole = await Role.findOne({where: {id: currentUser.roleId}});
    const conditions = currentUserRole.name === "teacher" ? {createdById: currentUser.id} : {};
    const studentRole = await Role.findOne({where: {name: 'student'}});
    const teacherRole = await Role.findOne({where: {name: 'teacher'}});

    const groupsCount = await Group.count({distinct: 'id', where: {...conditions}});
    const studentsCount = await User.count({distinct: 'id', where: {...conditions, roleId: studentRole.id}});
    const teachersCount = await User.count({distinct: 'id', where: {...conditions, roleId: teacherRole.id}});
    const tasksCount = await Task.count({distinct: 'id', where: {...conditions}});
    return res.status(200).json({
        responseOk: true,
        statistics: {groupsCount, studentsCount, teachersCount, tasksCount}
    });
}

exports.getLoggedInUserStudents = async (req, res, next) => {
    const currentUser = req.session.user;
    const currentUserRole = await Role.findOne({where: {id: currentUser.roleId}});
    const conditions = currentUserRole.name === "teacher" ? {createdById: currentUser.id} : {};
    const role = await Role.findOne({where: {name: 'student'}});
    const students = await User.findAll({
        where: {roleId: role.id, ...conditions},
        attributes: ['id', 'firstName', 'lastName']
    });
    const response = students.map(student => {
        return {value: student.id.toString(), label: `${student.firstName} ${student.lastName}`}
    });
    res.status(200).json({responseOk: true, students: response});
}

exports.getLoggedInUserGroups = async (req, res, next) => {
    const currentUser = req.session.user;
    const currentUserRole = await Role.findOne({where: {id: currentUser.roleId}});
    const conditions = currentUserRole.name === "teacher" ? {createdById: currentUser.id} : {};
    const groups = await Group.findAll({
        where: {...conditions},
        attributes: ['id', 'name']
    });
    const response = groups.map(group => {
        return {value: group.id.toString(), label: group.name}
    });
    res.status(200).json({responseOk: true, groups: response});
}

exports.getLoggedInUserGames = async (req, res, next) => {
    const currentUser = req.session.user;
    const currentUserRole = await Role.findOne({where: {id: currentUser.roleId}});
    const conditions = currentUserRole.name === "teacher" ? {createdById: currentUser.id} : {};
    const games = await Game.findAll({
        where: {...conditions},
        attributes: ['id', 'word', 'level']
    });
    const response = games.map(game => {
        return {value: game.id.toString(), label: `${game.word} (${game.level})`};
    });
    res.status(200).json({responseOk: true, games: response});
}

exports.getShowTasks = async (req, res, next) => {
    const currentUser = req.session.user;
    const currentUserRole = await Role.findOne({where: {id: currentUser.roleId}});
    const conditions = currentUserRole.name === "teacher" ? {createdById: currentUser.id} : {};
    const tasks = await Task.findAll({
        where: {...conditions},
        attributes: ['id', 'name', 'from', 'to', 'image', 'description']
    });
    res.status(200).json({responseOk: true, tasks});
}

exports.getShowGames = async (req, res, next) => {
    const currentUser = req.session.user;
    const currentUserRole = await Role.findOne({where: {id: currentUser.roleId}});
    const conditions = currentUserRole.name === "teacher" ? {createdById: currentUser.id} : {};
    const games = await Game.findAll({
        where: {...conditions},
        attributes: ['id', 'word', 'level']
    });
    res.status(200).json({responseOk: true, games});
}

exports.getTeachersForAdmin = async (req, res, next) => {
    const admin = req.session.user;
    const role = await Role.findOne({where: {name: 'teacher'}});
    const teachers = await User.findAll({
        where: {roleId: role.id}, attributes: ['id', 'firstName', 'lastName']
    });
    const response = teachers.map(teacher => {
        return {value: teacher.id.toString(), label: `${teacher.firstName} ${teacher.lastName}`}
    });

    // Add admin to teachers
    response.push({value: admin.id, label: `${admin.firstName} ${admin.lastName} (admin)`})
    res.status(200).json({responseOk: true, teachers: response});
}

exports.postCreateGroup = async (req, res, next) => {
    const groupName = req.body.groupName;
    const userIds = req.body.userIds;
    const ownerId = req.body.ownerId;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({responseOk: false, validationErrors: errors.array()})
    }

    const group = await Group.create({
        name: groupName,
        createdById: ownerId ? ownerId : req.session.user.id,
    });

    for (const uId of userIds) {
        const user = await User.findByPk(uId);
        await group.addUser(user);
        if (ownerId) {
            user.createdById = ownerId;
            await user.save()
        }
    }

    return res.status(200).json({responseOk: true});
}

exports.postCreateGame = async (req, res, next) => {
    const word = req.body.word;
    const row = req.body.row;
    const col = req.body.col;
    const level = req.body.level;
    const barrier = req.body.barrier;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({responseOk: false, validationErrors: errors.array()})
    }

    await Game.create({
        word: word,
        createdById: req.session.user.id,
        rows: row,
        columns: col,
        barrier: barrier,
        level: level
    });

    return res.status(200).json({responseOk: true});
}

exports.postEditGame = async (req, res, next) => {
    const gameId = req.body.gameId;
    const word = req.body.word;
    const row = req.body.row;
    const col = req.body.col;
    const level = req.body.level;
    const barrier = req.body.barrier;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({responseOk: false, validationErrors: errors.array()})
    }

    const game = await Game.findByPk(gameId);
    game.word = word;
    game.rows = row;
    game.columns = col;
    game.level = level;
    game.barrier = barrier;
    game.createdById = req.session.user.id;
    await game.save();

    return res.status(200).json({responseOk: true});
}

exports.postEditTask = async (req, res, next) => {
    const taskId = req.body.taskId;
    const title = req.body.title;
    const startedAt = req.body.startedAt;
    const finishedAt = req.body.finishedAt;
    const description = req.body.description;
    const gameIds = req.body.gameIds;
    const groupsIds = req.body.groupIds;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({responseOk: false, validationErrors: errors.array()})
    }

    const task = await Task.findByPk(taskId);
    task.name = title;
    task.from = startedAt;
    task.to = finishedAt;
    task.description = description;
    task.createdById = req.session.user.id;
    await task.save();

    const games = await Game.findAll({where: {id: {[Op.in]: gameIds}}});
    await task.setGames(games);

    const groups = await Group.findAll({where: {id: {[Op.in]: groupsIds}}});
    await task.setGroups(groups);

    const userGameGroups = await UserGame.findAll({ where: { taskId: task.id }, attributes: ['groupId']});
    const inputGroupIds = new Set(groupsIds.map(Number));
    const dbGroupIds = new Set(userGameGroups.map(g => g.groupId));
    const intersectionGroup = new Set([...inputGroupIds].filter(x => dbGroupIds.has(x)));
    const groupsToDelete = new Set([...dbGroupIds].filter(x => !intersectionGroup.has(x)));
    await UserGame.destroy({ where: { groupId: {[Op.in]: [...groupsToDelete]}, taskId: task.id } })

    groups.map(async(group) => {
        const users = await group.getUsers();
        users.map(async(user) =>{
            const inputGameIds = new Set(gameIds.map(Number));

            const actualUserGames = await UserGame.findAll({ where: { groupId: group.id, taskId: task.id, userId: user.id }, attributes: ['gameId'] })
            const dbGameIds = new Set(actualUserGames.map(g => g.gameId));

            const intersection = new Set([...inputGameIds].filter(x => dbGameIds.has(x)));
            const gamesToCreated = new Set([...inputGameIds].filter(x => !intersection.has(x)));
            const gamesToDelete = new Set([...dbGameIds].filter(x => !intersection.has(x)));

            [...gamesToCreated].map(async(gameId) => {
               await UserGame.create({
                   userId: user.id,
                   groupId: group.id,
                   gameId: gameId,
                   taskId: task.id,
               });
            });

            await UserGame.destroy({ where: { userId: user.id, groupId: group.id, taskId: task.id, gameId: {[Op.in]: [...gamesToDelete]} } });
        });
    });

    return res.status(200).json({responseOk: true});
}

exports.postCreateTask = async (req, res, next) => {
    const title = req.body.title;
    const startedAt = req.body.startedAt;
    const finishedAt = req.body.finishedAt;
    const description = req.body.description;
    const gameIds = req.body.gameIds;
    const groupsIds = req.body.groupIds;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({responseOk: false, validationErrors: errors.array()})
    }

    const task = await Task.create({
        name: title,
        from: startedAt,
        to: finishedAt,
        description: description,
        createdById: req.session.user.id
    });

    const games = await Game.findAll({where: {id: {[Op.in]: gameIds}}});
    await task.setGames(games);

    const groups = await Group.findAll({where: {id: {[Op.in]: groupsIds}}});
    await task.setGroups(groups);

    groups.map((group) => {
        group.getUsers().then(users => {
            users.map(u => {
                [...games].forEach(game => {
                   UserGame.create({
                       userId: u.id,
                       groupId: group.id,
                       gameId: game.id,
                       taskId: task.id,
                   }).catch(err => {
                       console.log(err)
                   });
                });
            });
        });
    });

    return res.status(200).json({responseOk: true});
}

exports.postEditGroup = async (req, res, next) => {
    const groupId = req.body.groupId;
    const groupName = req.body.groupName;
    const userIds = req.body.userIds;
    const ownerId = req.body.ownerId;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({responseOk: false, validationErrors: errors.array()})
    }
    const group = await Group.findByPk(groupId);
    group.name = groupName;
    group.createdById = ownerId ? ownerId : req.session.user.id;
    await group.save();

    const users = await User.findAll({where: {id: {[Op.in]: userIds}}});
    await group.setUsers(users);

    if (ownerId) {
        for (const user of users) {
            user.createdById = ownerId;
            await user.save();
        }
    }

    return res.status(200).json({responseOk: true});
}

exports.postDeleteGroup = (req, res, next) => {
    const groupId = req.body.id;
    Group.findByPk(groupId)
        .then(group => {
            return group.destroy();
        })
        .then(result => {
            res.status(200).json({responseOk: true})
        })
        .catch(err => console.log(err));
};

exports.postDeleteGame = (req, res, next) => {
    const gameId = req.body.id;
    Game.findByPk(gameId)
        .then(game => {
            return game.destroy();
        })
        .then(result => {
            res.status(200).json({responseOk: true})
        })
        .catch(err => console.log(err));
};

exports.postDeleteTask = (req, res, next) => {
    const taskId = req.body.id;

    Task.findByPk(taskId)
        .then(task => {
            return task.destroy();
        })
        .then(result => {
            res.status(200).json({responseOk: true})
        })
        .catch(err => console.log(err));
}

exports.getDashboard = (req, res, next) => {
    res.render('admin/dashboard', {
        pageTitle: 'Dashboard',
        path: '/admin/dashboard'
    });
}

exports.getGroupsPage = (req, res, next) => {
    res.render('admin/groups', {
        pageTitle: 'Skupiny',
        path: '/admin/groups'
    });
}

exports.getStudentsPage = (req, res, next) => {
    res.render('admin/students', {
        pageTitle: 'Žiaci',
        path: '/admin/students'
    });
}

exports.getTasksPage = (req, res, next) => {
    res.render('admin/tasksNew', {
        pageTitle: 'Úlohy',
        path: '/admin/tasks/new'
    });
}

exports.getTasksIndexPage = (req, res, next) => {
    res.render('admin/tasksIndex', {
        pageTitle: 'Úlohy',
        path: '/admin/tasks/index'
    });
}

exports.getTeachersPage = (req, res, next) => {
    res.render('admin/teachers', {
        pageTitle: 'Učitelia',
        path: '/admin/teachers'
    });
}

exports.getGamesPage = (req, res, next) => {
    res.render('admin/games', {
        pageTitle: 'Slová',
        path: '/admin/games/index'
    });
}
